@extends('templates.ssatemplate')
@section('content-script')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDmIUjqXxiTISW2wnpx77mGDB_VDddXZ28&sensor=false&callback=initialize"></script>
<script type="text/javascript" src="/js/mapa.js"></script>
<script type="text/javascript" src="/js/jquery-ui.custom.min.js"></script>

@endsection

@section('content')
<div class="row">
  @foreach ($iniciativa as $ini)
    <div  class=" col-md-9">
      <div class="initiative ">
        <h2>{{$ini->nome}}</h2>
        <p class="filtros"><img src="/images/site/pin-blue.png" class="icon-initiative" alt="">

                      @if ($ini->indice_setor === 1)
                        Público
                      @elseif ($ini->indice_setor === 2)
                        Privado
                      @else
                        Terceiro Setor
                      @endif
        <img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
          @if ($ini->indice_nivel_educacao === 1)
              Infantil
          @elseif ($ini->indice_nivel_educacao === 2)
              Fundamental
          @elseif ($ini->indice_nivel_educacao === 3)
              Médio
          @elseif ($ini->indice_nivel_educacao === 4)
              Profissional
          @else
              Superior
          @endif
         <img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
          @if ($ini->indice_forma_educacao === 1)
            Formal
          @elseif ($ini->indice_forma_educacao === 2)
            Não Formal
          @else
            Informal
          @endif
          </p>
        <i class="fa fa-map-signs" aria-hidden="true"></i><h5>{{$ini->endereco}}</h5>
      <br />
       <div class="box-image_">
        <img src="/images/projetos/{{$ini->avatar}}" class="img-fluid" id="myImg" alt="{{$ini->nome}}">
      </div>
      <br />

      <h4>O Que ?</h4>
      <p>{{$ini->definicao}} </p>
      <br />
      <h4>Por Que ?</h4>
      <p>{{$ini->motivo}}  </p>
      <br />
      <h4>Como ?</h4>
      <p>{{$ini->metodo}}</p>
      <br />
      <h4>Para Que?</h4>
      <p>{{$ini->finalidade}}</p>
      <br />
      <h4>Principais Resultados</h4>
      <p>{{$ini->resultado}}</p>
      <br>
      <div class="initiative-padding-bottom"></div>
      <div id="mapa" class="mapa-initiative"></div>
    </div>
  </div>
    <div class="col-md-3 col-sm-3">
        <input type="hidden" name="latitude" value="{{$ini->latitude}}" id="txtLatitude">
        <input type="hidden" name="longitude" value="{{$ini->longitude}}" id="txtLongitude">
        <p><h4  class="filtros_direita_h4"><i class="fa fa-calendar" aria-hidden="true"></i>
Tempo de Existência</h4></p>
        <p class="filtros_direita">
          {{$ini->tempo_existencia}}
        </p>
        <br />
        <p><h4 class="filtros_direita_h4"><i class="fa fa-money" aria-hidden="true"></i> Custo da Iniciativa</h4></p>
        <p class="filtros_direita">
          {{$ini->custo}}
        </p>
        <br />
        <p><h4 class="filtros_direita_h4"><i class="fa fa-line-chart" aria-hidden="true"></i> Alcance </p>
          <p class="filtros_direita">
            {{$ini->alcance}}
          </p>

        <br>
        <p ><h4 class="filtros_direita_h4"><i class="fa fa-list-alt" aria-hidden="true"></i>  Principais demandas da iniciativa</h4></p>
        <p class="filtros_direita">
          {{$ini->demandas}}
        </p>
        <br />
        <p ><h4 class="filtros_direita_h4"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Contribuição da iniciativa com o SPE</h4></p>
        <p class="filtros_direita">
          {{$ini->contribuicao}}
        </p>
        <br>
    </div>
    @endforeach
</div>

<!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">×</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>


<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}
</script>








@endsection
