<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Salvador Pela Educação</title>
    <link rel="shortcut icon" href="/images/site/favicon.png">
    <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>/ -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDutdA_w-BmH9fP-QiVxaU2a2mqydK7bSU" async defer></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>
    <!-- <script type="text/javascript" src="js/api/mapa/mapa.js"></script> -->
    <link rel="stylesheet" type="text/css" href="css/map.css" />
    <link rel="stylesheet" type="text/css" href="css/app.css" />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" type='text/css'>
    <?php
    $latitude = array();
    $longitude = array();
    $titulo = array();
    $avatar = array();

    foreach ($iniciativa as $ini){
      array_push($latitude, "$ini->latitude");
      array_push($longitude, "$ini->longitude");
      array_push($titulo,"$ini->nome");
      array_push($avatar, "$ini->avatar");
    }
     ?>
        <script type="text/javascript">
            var map;
            var i = 0;
            iniciativa = <?php echo json_encode($iniciativa); ?>;
            var lat = <?php echo json_encode($latitude); ?>;
            var long = <?php echo json_encode($longitude); ?>;
            var coordinates = [];
            var Coordinate = function (lat, lng) {
                this.lat = lat;
                this.lng = lng;
            }
            for (var i = 0; i < lat.length; i++) {
                coordinates.push(new Coordinate(lat[i], long[i]));
            }

            function addPlace(map, coordinate, iniciativa, i) {
                map.addMarker({
                    lat: coordinate.lat
                    , lng: coordinate.lng
                    , title: 'Marker with InfoWindow'
                    , infoWindow: {
                        content: '<div id="box-map" style="float:right;padding-left: 20px;"> \
            <h1 class="initiative-title"><a style="text-decoration: none;font-family: "Poppins", sans-serif;" target="_blank" href="/initiative/{{$ini->id}}">'+ iniciativa[i].nome +'</a> </h1> \
              </div>'
                    }
                    , icon: "images/site/pin_blue.png"
                });
                i++;
            }
            var cc = [];
            $(document).ready(function () {
                map = new GMaps({
                    el: '#map'
                    , lat: -12.994073
                    , lng: -38.5028423
                });
                i = 0;
                coordinates.forEach(function (coordinate) {
                    addPlace(map, coordinate, iniciativa, i);
                    i++;
                });
            });
        </script>
</head>

<body>
  <nav class="navbar navbar-default navbar-fixed-top menu-topo">
    <div class="container">
      <img id="logo-site" href="#"src="/images/site/logo-nome-azul.png" class="logo-menu" />
      <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle drop-menu" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>

          <div class="collapse navbar-collapse " id="myNavbar">
              <ul id="float-menu" class="nav navbar-nav opcoes-menu menu-principal">
              <li class="active opcoes-menu"><a href="/">Home</a></li>
              <li class="opcoes-menu"><a href="/about">Sobre</a></li>
              <li class="opcoes-menu"><a href="/actions">Ações</a></li>
              <li class="opcoes-menu"><a href="/map">Mapa</a></li>
              <li class="opcoes-menu"><a href="/whoweare">Quem Somos</a></li>
              <li class="opcoes-menu"><a href="/contact">Contato</a></li>
              <li><a href="https://www.facebook.com/salvadorpelaeducacao"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            </ul>
          </div>
      </div>
    </div>
  </nav>
    <div class="span11">
        <div id="map"></div>
    </div>
</body>

</html>
