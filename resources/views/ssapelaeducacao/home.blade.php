@extends('templates.ssatemplate')
@section('content-script')
<script src="http://npmcdn.com/isotope-layout@3/dist/isotope.pkgd.js" type="text/javascript"></script>
<script src="/js/initiative/filter.js" type="text/javascript"></script>
<link rel="stylesheet" href="/css/carousel/carousel.css">
@endsection

@section('carousel')
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @foreach ($carrosels as $car)
        <div class="item">
          <img class="first-slide" src="images/carrosel/{{$car->avatar}}" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1><?php echo $car->titulo; ?></h1>
              <p><?php echo $car->descricao; ?></p>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
<!--
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        @foreach ($carrosels as $car)
        <div class="item">
          <img class="first-slide" src="images/carrosel/{{$car->avatar}}" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1><?php echo $car->titulo; ?></h1>
              <p><?php echo $car->descricao; ?></p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
  </div>-->
@endsection

@section('content')
<br>
<br>
<div class="container">
  <div class="row">
    <div  class="col-sm-12 ">
      <div class="filters">
        <div class="fild">

          <legend>FILTRE POR INTERESSE: </legend>
          <div class="ui-group"> <img src="/images/site/mapa1.png" width="40" height="43 " alt="" />
            <h4>Setor</h4>
            <div class="button-group" id="filtro1" data-filter-group="setor">
              <button class="button is-checked" data-filter="">Todos</button> <span class="pipe">|</span>
              <button class="button" data-filter=".publico">Público</button> <span class="pipe">|</span>
              <button class="button" data-filter=".privado">Privado</button> <span class="pipe">|</span>
              <button class="button" data-filter=".terceiro">Terceiro</button>
            </div>
          </div>
          <div class="ui-group"> <img src="/images/site/mapa2.png" width="40" height="43 " alt="" />
            <h4>Formas de Educação</h4>
            <div class="button-group" id="filtro2" data-filter-group="formaeducacao">
              <button class="button is-checked" data-filter="">Todos</button> <span class="pipe">|</span>
              <button class="button" data-filter=".formal">Formal</button> <span class="pipe">|</span>
              <button class="button" data-filter=".naoformal">Não Formal</button> <span class="pipe">|</span>
              <button class="button" data-filter=".informal">Informal</button>
            </div>
          </div>
          <div class="ui-group"> <img src="/images/site/mapa3.png" width="40" height="43 " alt="" />
            <h4>Área de Atuação</h4>
            <div class="button-group" id="filtro3" data-filter-group="niveleducacao">
              <button class="button is-checked" data-filter="">Todos</button> <span class="pipe">|</span>
              <button class="button" data-filter=".artes">Artes Visuais</button> <span class="pipe">|</span>
              <button class="button" data-filter=".danca">Dança</button> <span class="pipe">|</span>
              <button class="button" data-filter=".musica">Música</button> <span class="pipe">|</span>
              <button class="button" data-filter=".esporte">Esporte</button> <span class="pipe">|</span>
              <button class="button" data-filter=".sustentabilidade">Sustentabilidade</button><span class="pipe">|</span>
              <button class="button" data-filter=".escolar">Escolar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div id="topo2" class="col-sm-12 col-xs-12">
      <div class="filters">
        <div class="fild">
          <legend>FILTRE POR INTERESSE: </legend>
          <div class="ui-group"> <img src="/images/site/mapa1.png" width="40" height="43 " alt="" />
            <h4>Setor</h4>
            <div class="button-group" id="filtro1" data-filter-group="setor">
              <button class="button is-checked" data-filter="">Todos</button>
              <button class="button btn btn-default" data-filter=".publico">Público</button>
              <button class="button btn btn-default" data-filter=".privado">Privado</button>
              <button class="button btn btn-default" data-filter=".terceiro">Terceiro</button>
            </div>
          </div>
          <div class="ui-group"> <img src="/images/site/mapa2.png" width="40" height="43 " alt="" />
            <h4>Formas de Educação</h4>
            <div class="button-group" id="filtro2" data-filter-group="formaeducacao">
              <button class="button is-checked" data-filter="">Todos</button>
              <button class="button btn btn-default" data-filter=".formal">Formal</button>
              <button class="button btn btn-default" data-filter=".naoformal">Não Formal</button>
              <button class="button btn btn-default" data-filter=".informal">Informal</button>
            </div>
          </div>
          <div class="ui-group"> <img src="/images/site/mapa3.png" width="40" height="43 " alt="" />
            <h4>Nível de Educação</h4>
            <div class="button-group" id="filtro3" data-filter-group="niveleducacao">
              <button class="button is-checked" data-filter="">Todos</button>
              <button class="button" data-filter=".infantil">Artes Visuais</button> <span class="pipe">|</span>
              <button class="button" data-filter=".fundamental">Dança</button> <span class="pipe">|</span>
              <button class="button" data-filter=".medio">Música</button> <span class="pipe">|</span>
              <button class="button" data-filter=".profissional">Esporte</button> <span class="pipe">|</span>
              <button class="button" data-filter=".superior">Sustentabilidade</button>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div id="info-search"> <span>{{$qtd}} experiências encontradas com os critérios escolhidos</span> </div>
<br />
<div class="container">
  <div class="row">
    <?php  $count = 0; $limite = 9; $total = $qtd; ?>
    @foreach ($iniciativa as $ini)
    @if($count < $limite ) <!-- (max-width: 1920px) and (min-width: 1153px) -->
    <div id="topo_initiative" class="col-md-4">
      <div class="grid" style="position: relative;">
        <div class="iniciativa {{$ini->indice_setor}} {{$ini->indice_nivel_educacao}} {{$ini->indice_forma_educacao}}" style="position: relative; left: 0px; top: 0px;">
          <a class="a-initiative" style="text-decoration: none;" href="/initiative/{{$ini->id}}">
            <article class="article-initiative">
              <div class="box-image"> <img src="/images/projetos/{{$ini->avatar}}" class="img-responsive img-initiative" role="presentation"> </div>
              <div class="block-initiative  ">
                <h3 class="title-block-initiative">{{$ini->nome}}</h3>
                <div class="end-initiative">
                  <p>{{$ini->endereco}}</p>
                </div>
                <div class="description-initiative">
                  <p><?php echo $ini->resume?>...</p>
                </div>
                <div class="filter-initiative"> <span class="filter-initiative-each">
                  <p><img src="/images/site/pin-blue.png" class="icon-initiative" alt="" >
                    @if ($ini->indice_setor == "publico")
                    Público
                    @elseif ($ini->indice_setor == "privado")
                    Privado
                    @else
                    Terceiro Setor
                    @endif
                  </p> </span>
                  <span class="filter-initiative-each">
                    <p><img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
                      @if ($ini->indice_nivel_educacao == 'artes')
                      Artes Visuais
                      @elseif ($ini->indice_nivel_educacao == 'danca')
                      Dança
                      @elseif ($ini->indice_nivel_educacao == 'musica')
                      Música
                      @elseif ($ini->indice_nivel_educacao == 'esporte')
                      Esporte
                      @elseif ($ini->indice_nivel_educacao == 'sustentabilidade')
                      Sustentabilidade
                      @else
                      Escolar
                      @endif  </p> </span>
                      <span class="filter-initiative-each">
                        <p>    <img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
                          @if ($ini->indice_forma_educacao == 'formal')
                          Formal
                          @elseif ($ini->indice_forma_educacao == 'naoformal')
                          Não Formal
                          @else
                          Informal
                          @endif
                        </p>
                      </span> </div>
                      <br />
                      <br /> </div>
                    </article>
                  </a>
                </div>
              </div>
            </div>
            <!-- (max-width: 1152px) and (min-width: 320px) -->
            <div id="topo_initiative_2" class="col-md-6">
              <div class="grid" style="postion: relative;">
                <div class="iniciativa {{$ini->indice_setor}} {{$ini->indice_nivel_educacao}} {{$ini->indice_forma_educacao}}" style="position: relative; left: 0px; top: 0px;">
                  <a class="a-initiative" style="text-decoration: none;" href="/initiative/{{$ini->id}}">
                    <article class="article-initiative">
                      <div class="box-image"> <img src="/images/projetos/{{$ini->avatar}}" class="img-responsive img-initiative" role="presentation"> </div>
                      <div class="block-initiative  ">
                        <h3 class="title-block-initiative">{{$ini->nome}}</h3>
                        <div class="end-initiative">
                          <p>{{$ini->endereco}}</p>
                        </div>
                        <div class="description-initiative">
                          <p><?php echo $ini->definicao?>...</p>
                        </div>
                        <div class="filter-initiative"> <span class="filter-initiative-each">
                          <p><img src="/images/site/pin-blue.png" class="icon-initiative" alt="" >
                            @if ($ini->indice_setor == "publico")
                            Público
                            @elseif ($ini->indice_setor == "privado")
                            Privado
                            @else
                            Terceiro Setor
                            @endif </p>  </span>
                            <span class="filter-initiative-each">
                              <p><img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
                                @if ($ini->indice_nivel_educacao == 'artes')
                                Artes Visuais
                                @elseif ($ini->indice_nivel_educacao == 'danca')
                                Dança
                                @elseif ($ini->indice_nivel_educacao == 'musica')
                                Música
                                @elseif ($ini->indice_nivel_educacao == 'esporte')
                                Esporte
                                @else
                                Sustentabilidade
                                @endif </p> </span>
                                <span class="filter-initiative-each">
                                  <p> <img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
                                    @if ($ini->indice_forma_educacao == 'formal')
                                    Formal
                                    @elseif ($ini->indice_forma_educacao == 'naoformal')
                                    Não Formal
                                    @else
                                    Informal
                                    @endif  </p>   </span>
                                  </div>
                                  <br />
                                  <br /> </div>
                                </article>
                              </a>
                            </div>
                          </div>
                        </div>
                        @endif
                        @endforeach
                      </div>
                    </div>

                    <!-- <div class="container">
                      <div class="row">
                        <?php  $count = 0; $limite = 9; $total = $qtd; ?>

                                          </div>
                                        </div> -->

                      <div class="container">
                        <div class="row seta">
                          {!! $iniciativa->appends(['sort' => 'votes'])->render() !!}
                          <!-- <i  class="fa fa-angle-down" id="proxima-pagina" onclick="expandir()"></i> -->
                        </div>
                      </div>
                      @endsection @section('footer-script')
                      <script type="text/javascript">

                      $(document).ready(function () {
                        $('#myCarousel').find('.item').first().addClass('active');
                      });
                      // external js: isotope.pkgd.js
                          var $grid = $('.grid').isotope({
                             itemSelector: '.iniciativa'
                             , layoutMode: 'fitRows'
                             , getSortData: {
                                 name: '.name'
                                 , symbol: '.symbol'
                                 , number: '.number parseInt'
                                 , category: '[data-category]'
                                 , weight: function (itemElem) {
                                     var weight = $(itemElem).find('.weight').text();
                                     return parseFloat(weight.replace(/[\(\)]/g, ''));
                                 }
                             }
                          });
                          $grid.on('arrangeComplete', function (event, filteredItems) {
                             console.log('arrangeComplete with ' + filteredItems.length + ' items');
                          });
                          // store filter for each group
                          var filters = {};
                          $('.filters').on('click', '.button', function () {
                             var $this = $(this);
                          //   get group key
                             var $buttonGroup = $this.parents('.button-group');
                             var filterGroup = $buttonGroup.attr('data-filter-group');
                          //   set filterfor group
                             filters[filterGroup] = $this.attr('data-filter');
                           //  console.log(filters[filterGroup]);
                          //   combine filters
                             var filterValue = concatValues(filters);
                             console.log(filterValue);
                           //  set filter for Isotope
                             $grid.isotope({
                                 filter: filterValue
                             });
                          });
                          //  change is - checked class on buttons
                          $('.button-group').each(function (i, buttonGroup) {
                             var $buttonGroup = $(buttonGroup);
                             $buttonGroup.on('click', 'button', function () {
                                 $buttonGroup.find('.is-checked').removeClass('is-checked');
                                 $(this).addClass('is-checked');
                             });
                          });
                          var elems = $grid.isotope('getItemElements')
                          //access Isotope properties console.log(elems) flatten object by concatting values
                          function concatValues(obj) {
                             var value = '';
                             for (var prop in obj) {
                                 value += obj[prop];
                             }
                             return value;
                          }
                      </script> @endsection
