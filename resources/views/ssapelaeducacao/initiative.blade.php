@extends('templates.ssatemplate') @section('content-script')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDmIUjqXxiTISW2wnpx77mGDB_VDddXZ28&sensor=false&callback=initialize"></script>
<script type="text/javascript" src="/js/mapa.js"></script>
<script type="text/javascript" src="/js/jquery-ui.custom.min.js"></script>
<link rel="stylesheet" href="/css/initiative.css"> @endsection @section('content')
<!-- container -->
<div class="container-fluid">
    <div class="row">
        <div class="espaco"></div> @foreach ($iniciativa as $ini)
        <div class="col-md-9">
            <h3 class="title-initiative">{{$ini->nome}}</h3>
            <p class="filtros filtross"><img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
                @if ($ini->indice_setor === 1)
                    Público
                @elseif ($ini->indice_setor === 2)
                    Privado
                @else
                    Terceiro Setor
                @endif
                <img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
                @if ($ini->indice_nivel_educacao == 'artes')
                  Artes Visuais
                @elseif ($ini->indice_nivel_educacao == 'danca')
                  Dança
                @elseif ($ini->indice_nivel_educacao == 'musica')
                  Música
                @elseif ($ini->indice_nivel_educacao == 'esporte')
                  Esporte
                @elseif ($ini->indice_nivel_educacao == 'sustentabilidade')
                    Sustentabilidade
                @else
                  Escolar
                @endif <img src="/images/site/pin-blue.png" class="icon-initiative" alt="">
                @if ($ini->indice_forma_educacao === 1)
                    Formal
                @elseif ($ini->indice_forma_educacao === 2)
                    Não Formal
                @else
                    Informal
                @endif </p>
            <div class="row">
              <br>
              <div class="col-8 col-md-1"></div>
              <div class="box-image_ my_img col-8 col-md-6"> <img src="/images/projetos/{{$ini->avatar}}" class=" img-responsive" id="img-initiative" alt="{{$ini->nome}}"> </div>
              <div class="col-8 col-md-1"></div>
            </div>

            <h3 class="about-initiative2">Sobre a iniciativa</h3>
            <div class="about-initiative">
                <p class="text-initiative"><?php echo $ini->definicao?> </p>
            </div>

            <div class="about-initiative2">
                <h3>Atividades desenvolvidas</h3>
                <p><?php echo $ini->atividades_desenvolvidas ?></p>
            </div>

            <div class="about-initiative2">
                <h3>Principais Resultados</h3>
                <p><?php echo $ini->resultado ?></p>
            </div>
            <div class="about-initiative2">
                <h3>Requisitos para participar</h3>
                <p><?php echo $ini->requisitos_participar ?></p>
            </div>
            <div class="about-initiative2">
                <h3>Como ajudar?</h3>
                <p>Ficou interessado pela iniciativa e deseja ajudar?</br> Saiba as principais demandas:</p>
                <p><?php echo $ini->demandas ?></p>
            </div>
        </div>
        <div class="col-md-3">
            <h3 class="">Localização no mapa</h3>
            <br>
            <div class="mapa">
                <div class="map google-maps">
                    <input type="hidden" name="latitude" value="{{$ini->latitude}}" id="txtLatitude">
                    <input type="hidden" name="longitude" value="{{$ini->longitude}}" id="txtLongitude">
                    <div id="mapa" class="mapa-initiative"></div>
                    <p>{{$ini->endereco}}</p>
                </div>

                </br>
                </br>
                <div class="contact-initiative">
                    <h3>Contato</h3></br>
                    <p>Telefone: {{$ini->contato}}</p>
                    <p>E-mail: {{$ini->email}}</p>
                    <p>Site: {{$ini->site}}</p>
                    <p>Facebook: {{$ini->facebook}}</p>
                </div>
                <div class="filters-initiative">
                    @if ($ini->tempo_existencia <= 5)
                      <div id="existencia_01"></div>
                    @elseif ($ini->tempo_existencia <= 6 && $ini->tempo_existencia <= 10)
                      <div id="existencia_02"></div>
                    @elseif ($ini->tempo_existencia <= 11 && $ini->tempo_existencia <= 15)
                      <div id="existencia_03"></div>
                    @elseif ($ini->tempo_existencia  > 15)
                      <div id="existencia_04"></div>

                    @endif
                   </div>
                   <br>
                <div class="filters-initiative">
                    @if ($ini->custo == 0)
                      <div id="custo_01"></div>
                    @elseif ($ini->custo >= 1 && $ini->custo <= 50)
                      <div id="custo_02"></div>
                    @elseif ($ini->custo >= 51 && $ini->custo <= 100)
                      <div id="custo_03"></div>
                      @elseif ($ini->custo > 100)
                        <div id="custo_04"></div>
                    @endif
                   </div>
                   <br>
                <div class="filters-initiative">
                    @if ($ini->alcance == 1)
                      <div id="alcance_01"></div>
                    @elseif ($ini->alcance == 2)
                      <div id="alcance_02"></div>
                    @elseif ($ini->alcance == 3)
                      <div id="alcance_03"></div>
                    @elseif ($ini->alcance == 4)
                      <div id="alcance_04"></div>
                    @elseif ($ini->alcance == 5)
                      <div id="alcance_05"></div>
                    @endif


                </div>
            </div> @endforeach </div>
    </div>
    <div id="myModal" class="modal"> <span class="close">×</span> <img class="modal-content" id="img01">
        <div id="caption"></div>
    </div>
    <script>
        // Get the modal
        var modal = document.getElementById('myModal');
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('myImg');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function () {
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }
            // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }
    </script>
