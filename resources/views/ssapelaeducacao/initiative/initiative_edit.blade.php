@extends('templates.ssatemplate') @section('extend')
<li><a href="/carrosel">Carrosel</a></li>
<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li> @endsection @section('content-script')
<script type="text/javascript" src="/js/mapa.js"></script>
<script type="text/javascript" src="/js/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDJOgSlPsYaplvGq7aU8qRgatgBB2Tk-FA"></script> @endsection @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form id="form-add" enctype="multipart/form-data" action="/edit" method="post">
                <input type="hidden" name="id" value="{{ $initiative->id }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="usr">Nome da Iniciativa:</label>
                    <input type="text" class="form-control" id="nome" name="nome" value="{{$initiative->nome}}"> </div>
                <div class="form-group">
                    <label for="txtEndereco">Endereço:</label>
                    <!-- <input type="text" class="form-control" id="end" name="end"> -->
                    <input type="text" class="form-control" id="endereco" name="endereco" value="{{$initiative->endereco}}" />
                    <input type="button" id="btnEndereco" name="btnEndereco" value="Mostrar no mapa" /> </div>
                <div id="mapa" style="height: 300px; width: 300px"></div>
                <br>
                <input type="file" name="image" />
                <br>
                <div class="form-group">
                    <label for="usr">Email:</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{$initiative->email}}"> </div>
                <div class="form-group">
                    <label for="usr">Site:</label>
                    <input type="text" class="form-control" id="site" name="site" value="{{$initiative->site}}"> </div>
                <div class="form-group">
                    <label for="usr">Perfil Facebook:</label>
                    <input type="text" class="form-control" id="facebook" name="facebook" value="{{$initiative->facebook}}"> </div>
                <div class="form-group">
                    <label for="usr">Contato (Telefone / Whatsapp):</label>
                    <input type="text" class="form-control" id="contato" name="contato" value="{{$initiative->contato}}"> </div>
                <div class="form-group">
                    <label for="usr">Número atual de organizadores:</label>
                    <input type="text" class="form-control" id="organizadores" name="organizadores" value="{{$initiative->organizadores}}"> </div>
                <div class="form-group">
                    <label for="usr">Número atual de alunos:</label>
                    <input type="text" class="form-control" id="alunos" name="alunos" value="{{$initiative->alunos}}"> </div>
                <div class="form-group">
                    <label for="usr">Tempo de Existência:</label>
                    <input type="text" class="form-control" id="tempo_existencia" name="tempo_existencia" value="{{$initiative->tempo_existencia}}"> </div>
                <div class="form-group">
                    <label for="usr">Custo da Iniciativa:</label>
                    <input type="text" class="form-control" id="custo" name="custo" value="{{$initiative->custo}}"> </div>
                <!-- <div class="form-group">
                    <label for="usr">Alcance:</label>
                    <input type="text" class="form-control" id="alcance" name="alcance" value="{{$initiative->alcance}}"> </div> -->
                    <div class="form-group">
                            <label name="alcance" for="comment">Alcance:</label>
                            <br />
                            <select name="alcance" class="c-select">
                                <option selected>Abra as opções</option>
                                <option value="1">1-50</option>
                                <option value="2">50-100</option>
                                <option value="3">100-500</option>
                                <option value="4">500-1000</option>
                                <option value="5">+1000</option>
                            </select>
                        </div>
                <div class="form-group">
                    <label for="comment">Tipo de atividade:</label>
                    <br />
                    <select name="tipo_atividade" class="c-select">
                        <option selected>Abra as opções</option>
                        <option value="cultura">Cultura</option>
                        <option value="formal">Ensino Formal</option>
                        <option value="esporte">Esporte</option>
                        <option value="outros">Outros</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="comment">Atuação da inciativa:</label>
                    <br />
                    <select name="atuacao_iniciativa" class="c-select">
                        <option selected>Abra as opções</option>
                        <option value="comunidade">Comunidade</option>
                        <option value="bairro">Bairro</option>
                        <option value="cidade">Cidades</option>
                        <option value="outros">Outros</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="comment">Sobre a iniciativa</label>
                    <textarea class="form-control" rows="5" id="definicao" name="definicao">{{$initiative->definicao}}</textarea>
                </div>

                <div class="form-group">
                    <label for="comment">Atividades desenvolvidas</label>
                    <textarea class="form-control" rows="5" id="atividades" name="atividades">{{$initiative->atividades_desenvolvidas}}</textarea>
                </div>

                <div class="form-group">
                    <label for="comment">Principais Resultados:</label>
                    <textarea class="form-control" rows="5" id="resultado" name="resultado">{{$initiative->resultado}}</textarea>
                </div>

                <div class="form-group">
                    <label for="comment">Setor:</label>
                    <br />
                    <select name="indice_setor" class="c-select">
                        <option>Abra as opções</option>
                        <option>Público</option>
                        <option>Privado</option>
                        <option>Terceiro Setor</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="comment">Forma de Educação:</label>
                    <br />
                    <select name="indice_forma_educacao" class="c-select">
                        <option>Abra as opções</option>
                        <option value="formal">Formal</option>
                        <option value="naoformal">Não Formal</option>
                        <option value="informal">Informal</option>
                    </select>
                </div>
                <div class="form-group">
                    <label name="formaeducacao" for="comment">Nível de Educação:</label>
                    <br />
                    <select name="indice_nivel_educacao" class="c-select">
                        <option selected>Abra as opções</option>
                        <option value="artes">Artes Visuais</option>
                        <option value="danca">Dança</option>
                        <option value="musica">Música</option>
                        <option value="esporte">Esporte</option>
                        <option value="sustentabilidade">Sustentabilidade</option>
                        <option value="escolar">Escolar</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="comment">Requisitos para participar:</label>
                    <textarea class="form-control" rows="5" id="requisitos_participar" name="requisitos_participar">{{$initiative->requisitos_participar}}</textarea>
                </div>
                <div class="form-group">
                    <label for="comment">Descrição do projeto (max. 170 caracteres):</label>
                    <textarea data-ls-module="charCounter" maxlength="170" class="form-control" rows="5" id="resume" name="resume">{{$initiative->resume}}</textarea>
                </div>
                <div class="form-group">
                    <label for="comment">Principais demandas da iniciativa:</label>
                    <textarea class="form-control" rows="5" id="demandas" name="demandas">{{$initiative->demandas}}</textarea>
                </div>
                <div class="form-group">
                    <label for="comment">Contribuição da iniciativa com o SPE:</label>
                    <textarea class="form-control" rows="5" id="contribuicao" name="contribuicao">{{$initiative->contribuicao}}</textarea>
                </div>
                <button type="submit" value="Enviar" name="btnEnviar" class="btn btn-primary">Enviar</button>
                <input type="hidden" id="txtLatitude" name="txtLatitude" />
                <input type="hidden" id="txtLongitude" name="txtLongitude" /> </form>
        </div>
    </div>
</div>
<script type="text/javascript" >
    window.onload = function()  {
        CKEDITOR.replace('definicao');
        CKEDITOR.replace('atividades');
        CKEDITOR.replace('resultado');
        CKEDITOR.replace('requisitos_participar');
        CKEDITOR.replace('resume');
        CKEDITOR.replace('demandas');
        CKEDITOR.replace('contribuicao');
    };
</script>
@endsection
