@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Iniciativas</h2>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody> @foreach ($initiative as $ini)
                    <tr>
                        <td>{{$ini->nome}}</td>
                        <td>
                            <a href="{{ url('/initiative', ['id'=>$ini->id]) }}">
                                <button id="btnAdm" class="btn btn-sucess fa fa-eye " accesskey="a" hidden>
                                    <br>Visualizar</button>
                            </a>
                            <a href="{{ url('/initiative_edit', ['id'=>$ini->id]) }}">
                                <button id="btnAdm" class="btn btn-lg btn-warning fa fa-pencil" accesskey="a" hidden>
                                    <br>Editar</button>
                            </a>
                            <a  onclick="javascript:if(!confirm('Deseja excluir??'))return false;" href="{{ url('/initiative_destroy', ['id'=>$ini->id]) }}">
                                <button id="btnAdm"  class="btn btn-danger fa fa-trash" accesskey="a" hidden>
                                    <br>Deletar</button>
                            </a>
                        </td>
                    </tr> @endforeach </tbody>
            </table>
        </div>
    </div>
</div> @endsection