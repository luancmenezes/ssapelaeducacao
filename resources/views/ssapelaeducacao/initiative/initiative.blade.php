@extends('templates.carrosel') @section('main')
<h2>Iniciativas</h2>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody> @foreach ($initiative as $ini)
        <tr>
            <td>{{$ini->nome}}</td>
            <td>
                <a href="{{ url('/initiative', ['id'=>$ini->id]) }}">
                    <button id="btnEdit" class="btn btn-sucess " accesskey="a" hidden>
                        <br>Visualizar</button>
                </a>
                <a href="{{ url('/initiative_edit', ['id'=>$ini->id]) }}">
                    <button id="btnEdit" class="btn btn-lg btn-warning fa fa-pencil" accesskey="a" hidden>
                        <br>Editar</button>
                </a>
                <a href="{{ url('/delete', ['id'=>$ini->id]) }}">
                    <button id="btnEdit" class="btn btn-danger " accesskey="a" hidden>
                        <br>Deletar</button>
                </a>
            </td>
        </tr> @endforeach </tbody>
</table> @endsection