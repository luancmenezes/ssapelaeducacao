@extends('templates.ssatemplate')
@section('content-script')
<link rel="stylesheet" href="/css/contact/contact.css">
@endsection
@section('content')
    <div class="row">
        <div class="contact">
            <div class="col-xs-12 col-md-4">
                <h3 id="inscricao">Quer fazer parte da nossa rede? Se inscreva aqui</h3> <p>
                  <a class="embedly-card" href="https://salvadorpelaeducacao.typeform.com/to/tbB2Yg">
                    <p id="inscricao">
                      Inscrição para voluntários
                    </p>
                  </a>
                </p>
                <br/><br/>
                <h3 id="inscricao">Quero cadastrar minha iniciativa educacional no mapa</h3>
            </div>
        </div>

        <div class="col-xs-12 col-md-8">
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div> @endif
            <div class="contact">
                <h1>Fale Conosco</h1>
                <h5><i class="fa fa-envelope" aria-hidden="true"></i> salvadorpelaeducacao@gmail.com</h5>
               @unless($errors->isEmpty())
                <div class="alert alert-danger">
                    <ul> @foreach($errors->getMessages() as $error)
                        <li>{{ $error[0] }}</li> @endforeach </ul> @endunless </div>
                <form action="/email" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" required> </div>
                    <div class="form-group">
                        <label for="email">E-Mail</label>
                        <input type="text" id="email" name="email" class="form-control" placeholder="E-Mail" required> </div>
                    <div class="form-group">
                        <textarea id="mensagem" name="mensagem" class="form-control" rows="7" placeholder="Digite sua mensagem" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>

@endsection
