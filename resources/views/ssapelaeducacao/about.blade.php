@extends('templates.ssatemplate') @section('content-script') @endsection @section('content')
<script>
    function mostra(id) {
        var array = new Array('tela-1', 'tela-2', 'tela-3', 'tela-4');
        x = document.getElementById(id)
        for (i = 0; i < array.length; i++) {
            if (array[i] == id) {
                x.style.display = 'block';
            }
            else {
                document.getElementById(array[i]).style.display = 'none';
            }
        }
    }
</script>

<!-- menu-lateral -->
<div class="row" style="padding:0px;">
    <aside class="col-xs-12 col-md-4 menu-sobre-projeto">
        <h1 class="acoes">SOBRE</h1>
        <ul class="menu-lateral">
            <li class="sobre-menu texto-menu"><a onclick="mostra('tela-1');" style="cursor: hand;" class="item-1 ">Salvador Pela Educação</a></li>
            <li class="sobre-menu texto-menu"><a onclick="mostra('tela-2');" style="cursor: hand;" class="item-2 ">O que é?</a></li>
            <li class="sobre-menu texto-menu"><a onclick="mostra('tela-3');" style="cursor: hand;" class="item-3">Nosso conceito de educação</a></li>
        </ul>
    </aside>

    <main class="col-xs-12 col-md-8 conteudo-sobre-projeto">
        <br>
        <!-- sobre-faz-sentido -->
        <div id="tela-1" class="telas-menu sobre-faz-sentido col-xs-12 col-md-12">
              <img src="/images/site/ssapelaeducacao.png" class="img-circle img-sobre" alt="" />
        </div>

        <!-- sobre-o-que-e tela-2-->
        <div style="display:none;" id="tela-2" class="telas-menu col-xs-12 col-md-12">
              <h3 class="topicos-sobre">O que é?</h3>
              <br>
              <p align="justify"> O Salvador Pela Educação é um movimento criado para construir pontes entre iniciativas, recursos e pessoas no campo da EDUCAÇÃO. Acreditamos que facilitar o diálogo e criar conexões entre pessoas que trabalham ou se interessam por educação na cidade potencializa as ações educacionais do município e, consequentemente, melhora a qualidade da educação local. Nossos principais objetivos são:
                  <br><br>
                  1) Chamar atenção para o tema Educação no município de Salvador.</br></br>
                  2) Conectar os cidadãos soteropolitanos às iniciativas educacionais da cidade.</br></br>
                  3) Articular as iniciativas de educação entre si para potencializar suas ações."</br>
              </p>
              <br><br><br><br>
        </div>

        <!-- sobre-o-que-e tela-3-->
        <div style="display:none;" id="tela-3" class="telas-menu col-xs-12 col-md-12">
            <h3 class="topicos-sobre">Nosso conceito de educação</h3>
            <p align="justify">As iniciativas educacionais que estão contempladas pelo nosso conceito de educação são as mais diversas possíveis. Buscamos atingir todas as faixas etárias (da creche ao pós-doutorado) e adotamos o conceito que classifica a educação em 3 tipos:</p>
            <p align="justify">Formal: acontece nas instituições educativas que seguem um currículo regido pela legislação, como por exemplo escolas, universidades e institutos federais. </p>
            <p align="justify">Não formal: acontece em locais que não necessitam seguir o currículo legal, como por exemplo museus, centros culturais e galerias.</p>
            <p align="justify">Informal:acontece em locais não predestinados à educação, desde discussões entre amigos no restaurante até cortejos nas praças e movimentos sociais pelas ruas.</p>
            <p align="justify"> Para nós, o processo educativo se dá dentro do educando e em sua relação com o mediador, que pode ser um professor, um livro ou uma máquina. Educar, partindo de uma de suas raízes etimológicas, significa extrair, ou seja, apresentar ao mundo as habilidades que o indivíduo construiu. É trazer algo novo ao ser humano. É lhe dar a oportunidade de ser alguém melhor para si e para o mundo. É por isso que, para além de qualquer outro significado, o Salvador pela Educação entende que educar é um ato de amor.</p>
        </div>
    </main>
</div> @endsection
