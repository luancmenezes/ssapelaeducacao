<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Google Maps API v3: Busca de endereço e Autocomplete - Demo</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:600" type="text/css" rel="stylesheet" />
    <link href="/css/estilo.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyADIeNquDm4R3xFxjY18L871MxmERbPKHM&amp;sensor=false"></script>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/mapa.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.custom.min.js"></script>
</head>

<body>
    <div id="apresentacao">
        <h1>Google Maps API v3: Busca de endereço e Autocomplete - Demo</h1>
        <form>
            <fieldset>
                <legend>Google Maps API v3: Busca de endereço e Autocomplete - Demo</legend>
                <label for="txtEndereco">Endereço:</label>
                <input type="text" id="txtEndereco" name="end" />
                <input type="button" id="btnEndereco" name="Endereco" value="Mostrar no mapa" />
                <div id="mapa"></div>
                <input type="submit" value="Enviar" name="btnEnviar" />
                <input type="hidden" id="txtLatitude" name="txtLatitude" />
                <input type="hidden" id="txtLongitude" name="txtLongitude" /> </fieldset>
        </form>
    </div>
</body>

</html>