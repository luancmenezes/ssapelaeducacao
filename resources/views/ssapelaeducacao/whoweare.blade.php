@extends('templates.ssatemplate') @section('content-script')
<script type="text/javascript" src="/js/modernizr.custom.79639.js"></script>
<link rel="stylesheet" type="text/css" href="/css/style6.css">
<link rel="stylesheet" type="text/css" href="/css/demo.css">
<link rel="stylesheet" type="text/css" href="/css/common.css"> @endsection @section('content')
<div>
  <div class="container">
    <div class="row">
      <section class="col-md-6">
        <div class="description">
          <h1>Quem Somos</h1>
          <br />
          <br />
          <p> Cidadãs e cidadãos soteropolitanos das mais diversas áreas de atuação profissional (Educação, Economia, Administração, Direito, etc.) que se interessam pela melhoria da qualidade da educação na cidade. A equipe se conheceu na organização da Virada Educação 2015 e foi lá que perceberam que tinham um propósito em comum: contribuir para a transformação da cidade por meio da educação. O grupo tem como uma de suas principais características o desejo constante de atuar ao lado das comunidades, onde a educação acontece em sua essência. </p>
        </div>
      </section>
      <section class="col-md-6">
        <div id="img-whoweare"> <img src="/images/site/ssapelaeducacao.png" class="img-responsive img-circle" alt="Cinque Terre" width="304" height="236"> </div>
      </section>
    </div>
  </div>
  <br></br>
  <br></br>
  <div class="container">
    <div class="row">
      <h2 class="title-whoweare">Conheça melhor os cofundadores do Salvador Pela Educação:</h2>
      <div class="col-md-4">
        <div class="members">
          <ul class="ch-grid">
            <li>
              <a style="cursor:pointer;" id="person1">
                <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/1.png"> </div>
              </a>
              <h3 class="h3color">Alan Carvalho</h3>
              <p style="font-size:12px;" class="cris">Cofundador e Presidente</p>
            </li>
          </ul>
          <!-- Modal -->
          <div id="myModal1" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
              <div class="modal-header"> <span class="close">×</span>
                <h2 class="titulo-modal">Alan Carvalho</h2> </div>
                <div class="modal-body">
                  <p class="litlebio biografia">Natural de Salvador, Alan tem 26 anos e é um apaixonado por educação da cabeça aos pés. Formou-se em engenharia mecatrônica pela USP, fez cursos de psicologia e educação na CSU-Chico e estágio acadêmico no Centro Lemann em Stanford. Voltando ao Brasil atuou com formação de professores na Neofuturo, como gestor de operações. No terceiro setor, foi gerente de projeto no LABi e, recentemente atuou na expansão da plataforma de gestão educacional da Studo+. Alan Carvalho é a mente criativa que idealizou o Salvador Pela Educação.

                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="members">
            <ul class="ch-grid">
              <li>
                <a style="cursor:pointer;" id="person2">
                  <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/2.jpg"> </div>
                </a>
                <h3 class="h3color">Laíse Santos</h3>
                <p style="font-size:12px;" class="cris">Cofundadora e Gerente de Projetos</p>
              </li>
            </ul>
            <!-- Modal -->
            <div id="myModal2" class="modal">
              <!-- Modal content -->
              <div class="modal-content">
                <div class="modal-header"> <span class="close">×</span>
                  <h2 class="titulo-modal">Laíse Santos</h2> </div>
                  <div class="modal-body">
                    <p class="litlebio biografia">Radicada em Salvador, 21 anos, graduanda em Economia pela UFBA. Já trabalhou com Administração Financeira no setor privado e Gestão de Fundos no governo da Bahia. Desde 2013, já assumiu cargos de liderança e alto impacto no Rotary International, compôs o Global Shapers Hub Salvador da Bahia, foi Co-organizer e Diretora de Logística no TEDxRioVermelho, organiza a Virada Educação Salvador, é embaixadora do Ensina Brasil, atuou como Multiplicadora e Mentora do Pense Grande - Fundação Telefônica Vivo e participa como Mobilizadora no Programa ProLíder. Amante da cultura e apaixonada pelo mundo dos projetos, nas horas vagas desbrava as informações infinitas que o universo oferece. Está no Salvador Pela Educação desde os tempos de ideação do movimento.

                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-4">
            <div class="members">
              <ul class="ch-grid">
                <li>
                  <a style="cursor:pointer;" id="person3">
                    <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/3.png"> </div>
                  </a>
                  <h3 class="h3color">Matheus Caldas</h3>
                  <p style="font-size:12px;" class="cris">Cofundador e Gerente de Projetos</p>
                </li>
              </ul>
              <!-- Modal -->
              <div id="myModal3" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                  <div class="modal-header"> <span class="close">×</span>
                    <h2 class="titulo-modal">Matheus Caldas</h2> </div>
                    <div class="modal-body">
                      <p class="litlebio biografia">Aos 24 anos, natural de Salvador, formado em Administração pela Universidade do Estado da Bahia, Matheus está desde os primeiros encontros de formação do Salvador pela Educação, realiza trabalho voluntário pelo Rotaract, foi Diretor de Programação do TEDxRioVermelho e tem a certeza que o altruísmo é a principal base para a construção de um mundo melhor.

                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="members">
                <ul class="ch-grid">
                  <li>
                    <a style="cursor:pointer;" id="person4">
                      <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/4.jpg"> </div>
                    </a>
                    <h3 class="h3color">Janaísa Viscardi</h3>
                    <p style="font-size:12px;" class="cris">Cofundadora e Conselheira</p>
                  </li>
                </ul>
                <!-- Modal -->
                <div id="myModal4" class="modal">
                  <!-- Modal content -->
                  <div class="modal-content">
                    <div class="modal-header"> <span class="close">×</span>
                      <h2 class="titulo-modal">Janaísa Viscardi</h2> </div>
                      <div class="modal-body">
                        <p class="litlebio biografia">Doutora em Linguística pela UNICAMP, Jana sempre viu na linguagem o caminho para a disseminação de conhecimento e a busca por um mundo de maior compaixão e empatia. É professora, já atuou como coordenadora de projetos educacionais internacionais para jovens de escola técnica e universitários e mantém um canal no Youtube para falar de temas relacionados à linguagem e ao empoderamento feminino.

                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="members">
                  <ul class="ch-grid">
                    <li>
                      <a style="cursor:pointer;" id="person5">
                        <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/5.png"> </div>
                      </a>
                      <h3 class="h3color">Ricardo Henriques</h3>
                      <p style="font-size:12px;" class="cris">Cofundador e Gerente de Finanças</p>
                    </li>
                  </ul>
                  <!-- Modal -->
                  <div id="myModal5" class="modal">
                    <!-- Modal content -->
                    <div class="modal-content">
                      <div class="modal-header"> <span class="close">×</span>
                        <h2 class="titulo-modal">Ricardo Henriques</h2> </div>
                        <div class="modal-body">
                          <p class="litlebio biografia">Natural de Salvador,24 anos, advogado formado pela Universidade Católica do Salvador, fã de música e interessado em qualquer forma de manifestação cultural. Foi Diretor Financeiro do TEDxRioVermelho, atualmente desenvolve além da área de finanças o projeto de Mapeamento das Iniciativas Educacionais do Salvador pela Educação.  Ricardo é curioso por conhecer a história de vida das outras pessoas.

                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="members">
                    <ul class="ch-grid">
                      <li>
                        <a style="cursor:pointer;" id="person6">
                          <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/6.png"> </div>
                        </a>
                        <h3 class="h3color">Angra Valesca</h3>
                        <p style="font-size:12px;">Cofundador</p>
                      </li>
                    </ul>
                    <!-- Modal -->
                    <div id="myModal6" class="modal">
                      <!-- Modal content -->
                      <div class="modal-content">
                        <div class="modal-header"> <span class="close">×</span>
                          <h2 class="titulo-modal">Angra Valesca</h2> </div>
                          <div class="modal-body">
                            <p class="litlebio biografia">Nascida em São Paulo, Angra Valesca, 24 anos, é formanda em Psicologia pela Universidade Federal da Bahia. Já realizou estágio em Recrutamento e Seleção pelo SESC Bahia, participou de projetos de educação para carreira; foi Diretora de Qualidade do TEDxRioVermelho, atualmente é professora de educação infantil. Angra, é a moça das dinâmicas dentro do Salvador Pela Educação.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="members">
                      <ul class="ch-grid">
                        <li>
                          <a style="cursor:pointer;" id="person7">
                            <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/7.png"> </div>
                          </a>
                          <h3 class="h3color">Ricardo Penido</h3>
                          <p style="font-size:12px;" class="cris">Gerente de Marketing</p>
                        </li>
                      </ul>
                      <!-- Modal -->
                      <div id="myModal7" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content">
                          <div class="modal-header"> <span class="close">×</span>
                            <h2 class="titulo-modal">Ricardo Penido</h2> </div>
                            <div class="modal-body">
                              <p class="litlebio biografia">Aos 19 anos, natural de Salvador, cursa o Bacharelado Interdisciplinar em Humanidades na UFBA. Participou da organização da Virada Educação Salvador. Brincante, acredita na criança como fonte de alegria e inspiração. Dedica-se a projetos sociais desde os 15 anos, confia na poesia e na transformação do mundo.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="members">
                        <ul class="ch-grid">
                          <li>
                            <a style="cursor:pointer;" id="person8">
                              <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/8.png"> </div>
                            </a>
                            <h3 class="h3color">Paula Fidalgo</h3>
                            <p style="font-size:12px;" class="cris">Cofundadora</p>
                          </li>
                        </ul>
                        <!-- Modal -->
                        <div id="myModal8" class="modal">
                          <!-- Modal content -->
                          <div class="modal-content">
                            <div class="modal-header"> <span class="close">×</span>
                              <h2 class="titulo-modal">Paula Fidalgo</h2> </div>
                              <div class="modal-body">
                                <p class="litlebio biografia">Soteropolitana, 23 anos, graduanda em Arquitetura pela Universidade Federal da Bahia. Paula fez parte da Diretoria de Logística do TEDxRioVermelho e atua há anos no CISV International, promovendo a Cultura da Paz. Uma amante das Artes e do mão na massa, Paula está na área de marketing digital e criações audiovisuais do Salvador Pela Educação, sempre cuidando para que o colorido e a leveza permaneçam no movimento.

                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="members">
                          <ul class="ch-grid">
                            <li>
                              <a style="cursor:pointer;" id="person9">
                                <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/9.png"> </div>
                              </a>
                              <h3 class="h3color">Cristiano Lima Nascimento</h3>
                              <p style="font-size:12px;" >Cofundador</p>
                            </li>
                          </ul>
                          <!-- Modal -->
                          <div id="myModal9" class="modal">
                            <!-- Modal content -->
                            <div class="modal-content">
                              <div class="modal-header"> <span class="close">×</span>
                                <h2 class="titulo-modal">Cristiano Lima Nascimento</h2> </div>
                                <div class="modal-body">
                                  <p class="litlebio biografia">Aos 23 anos, natural de Salvador, estudante de Medicina na Escola Bahiana de Medicina e Saúde Pública, adepto de movimentos locais, nacionais e internacionais de capacitação, assistencialismo e auxílio à comunidades e impacto social. Cristiano é Presidente e fundador da Liga Acadêmica de Psiquiatria (LAP), membro das organizações Rotaract, Global Shapers e CISV International. Cristiano divide seus dias entre a medicina e a paixão por projetos para ajudar o próximo. </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="members">
                            <ul class="ch-grid">
                              <li>
                                <a style="cursor:pointer;" id="person10">
                                  <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/10.png"> </div>
                                </a>
                                <h3 class="h3color">Fernanda Gusmão</h3>
                                <p style="font-size:12px;">Cofundadora</p>
                              </li>
                            </ul>
                            <!-- Modal -->
                            <div id="myModal10" class="modal">
                              <!-- Modal content -->
                              <div class="modal-content">
                                <div class="modal-header"> <span class="close">×</span>
                                  <h2 class="titulo-modal">Fernanda Gusmão</h2> </div>
                                  <div class="modal-body">
                                    <p class="litlebio biografia">Fernanda tem 22 anos, é estudante de Administração da Universidade Federal da Bahia, vice-presidente da ONG Creche Cantinho Encantado, Gerente de Projetos de intercâmbios sociais da AIESEC Salvador. No Salvador Pela Educação, Fernanda é a moça dos contatos e dos voluntários. Apaixonada pelo Terceiro setor, educação, economia solidária e empreendedorismo.
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="members">
                              <ul class="ch-grid">
                                <li>
                                  <a style="cursor:pointer;" id="person11">
                                    <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/11.png"> </div>
                                  </a>
                                  <h3 class="h3color">Henrique Souza Santos</h3>
                                  <p style="font-size:12px;">Cofundador</p>
                                </li>
                              </ul>
                              <!-- Modal -->
                              <div id="myModal11" class="modal">
                                <!-- Modal content -->
                                <div class="modal-content">
                                  <div class="modal-header"> <span class="close">×</span>
                                    <h2 class="titulo-modal">Henrique Souza Santos</h2> </div>
                                    <div class="modal-body">
                                      <p class="litlebio biografia">Henrique é nascido em Salvador, 21 anos e estudante de Medicina na Escola Bahiana de Medicina. Membro fundador da Liga Acadêmica de Medicina Generalista e da Liga Acadêmica de Psiquiatria bem como membro da linha de pesquisa Comportamento e Aprendizado Motor. Foi Diretor de Captação de Recursos do TEDxRioVermelho. Henrique é o rapaz do networking interinstitucional e um fascinado por iniciativas de cunho colaborativo. </p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="members">
                                <ul class="ch-grid">
                                  <li>
                                    <a style="cursor:pointer;" id="person12">
                                      <div class="box-image_whoweare"> <img class="ch-item" src="images/quemsomos/12.jpg"> </div>
                                    </a>
                                    <h3 class="h3color">Catherine Mainart</h3>
                                    <p style="font-size:12px;">Cofundadora</p>
                                  </li>
                                </ul>
                                <!-- Modal -->
                                <div id="myModal12" class="modal">
                                  <!-- Modal content -->
                                  <div class="modal-content">
                                    <div class="modal-header"> <span class="close">×</span>
                                      <h2 class="titulo-modal">Catherine Mainart</h2> </div>
                                      <div class="modal-body">
                                        <p class="litlebio biografia">Aos 18 anos,cursa o Bacharelado Interdisciplinar em Humanidades na UFBA. Sempre gostou de trabalhar com educação e acredita que esta seja a melhor ferramenta de transformação social. Idealizou um projeto relacionado a educação na época do colégio, o NAIC ( Núcleo Acadêmico de Incentivo ao Conhecimento), participou da organização do TEDxRioVermelho e da Virada Educação Salvador 2016, já foimembro do CISV e atualmente é embaixadora do Mapa Educação na Bahia. Considera-se uma pessoa bem eclética, já fez cursos de astronomia e astrofísica à ciência política, hoje trabalha com comunicação social e divulgação científica.</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>



                              <br></br>
                              <br></br>
                              <br></br>
                              <br></br>
                            </div>
                          </div>
                        </div>
                        <script type="text/javascript" src="/js/modals.js"></script>
                        @endsection
