@extends('templates.ssatemplate') @section('content')
<script>
    function mostra(id) {
        var array = new Array('tela-1', 'tela-2', 'tela-3', 'tela-4');
        x = document.getElementById(id)
        for (i = 0; i < array.length; i++) {
            if (array[i] == id) {
                x.style.display = 'block';
            }
            else {
                document.getElementById(array[i]).style.display = 'none';
            }
        }
    }
</script>

<div class="row" style="padding:0px;">
    <aside class="col-xs-12 col-md-4 menu-sobre-projeto">
        <h1 class="acoes">AÇÕES</h1>
        <ul class="menu-lateral">
            <li class="sobre-menu texto-menu"><a onclick="mostra('tela-1');" style="cursor: hand;" class="item-1">Mapeamento de inciativas educacionais</a></li>
            <li class="sobre-menu texto-menu"><a onclick="mostra('tela-2');" style="cursor: hand;" class="item-2">TEDxRioVermelho</a></li>
            <li class="sobre-menu texto-menu"><a onclick="mostra('tela-3');" style="cursor: hand;" class="item-3">Virada Educação Salvador</a></li>
            <li class="sobre-menu texto-menu"><a onclick="mostra('tela-4');" style="cursor: hand;" class="item-4">Saiba mais</a></li>
        </ul>
    </aside>

    <main class="col-xs-12 col-md-8 conteudo-sobre-projeto">
        <div id="tela-1" class="telas-menu col-xs-12 col-md-12" >
            <h1 class="topicos-sobre">Mapeamento de Iniciativas Educacionais</h1>
            <div class="col-xs-12 col-md-8">
              <p align="justify">
                Você sabe onde fica a escola pública mais próxima da sua casa? Você quer ajudar uma iniciativa
                em educação, mas não conhece algo perto da sua casa? Ou queria ajudar uma ong que atue num determinado tema de
                educação do seu interesse, mas não conhece algo relacionado em Salvador?<br><br>
                Para responder as questões acima, percebemos a necessidade de conhecer de perto as iniciativas
                educacionais da cidade e mostrá-las para a comunidade soteropolitana, surge, então, o Mapeamento de Iniciativas
                Educacionais.
              </p>
            </div>
            <img align-content="center" class="img_descricao_ted" src="/images/acao/mapeamento-action.png" alt="Cinque Terre" width="270" height="250">
            <div class="col-xs-12 col-md-12">
              <p align="justify">A intenção é criar um mapa colaborativo da educação de Salvador. Para fazer parte do mapa,
                a iniciativa
                precisará respeitar os seguintes critérios:
              </p>
              <ul>
                <li>Continuidade: Ser uma ação que dure mais de 1 ano.</li>
                <li>Acessibilidade: Preço abaixo do mercado.</li>
                <li>Colaboração: As iniciativas públicas serão incluídas dada a sua utilidade. As iniciativas privadas que quiserem colaborar com outras iniciativas são bem-vindas ao mapa.</li>
              </ul><br>
              <p align="justify">A fase inicial do Mapeamento de Iniciativas Educacionais percorreu diversas localidades do
                município de Salvador. Foi levando em consideração uma dispersão estratégica dessas regiões pelo território
                soteropolitano. Entre os meses de novembro de 2015 e janeiro de 2016, grupos com, no mínimo, 3 integrantes
                realizaram visitas nos locais escolhidos (descritos abaixo), para conhecer as atividades educacionais presentes na
                capital baiana.
              </p>
              <p align="justify">As regiões pioneiras para o  Mapeamento foram:</p>
              <ul>
                <li>Boca do Rio;</li>
                <li>Cabula;</li>
                <li>Candeal;</li>
                <li>Brotas;</li>
                <li>Cajazeiras;</li>
                <li>Campo Grande;</li>
                <li>Coutos;</li>
                <li>Itapuã;</li>
                <li>Pelourinho;</li>
                <li>Plataforma;</li>
                <li>Ribeira.</li>
              </ul><br>
              <p align="justify">Ao término da primeira fase do mapeamento, todas as informações (funcionamento, público-alvo, alcance, demandas das
                iniciativas, etc) das dezenas de iniciativas coletadas foram cadastradas e serviram de base de dados para o Mapa
                Educacional presente aqui no site do Salvador Pela Educação que é aberto para todo e qualquer cidadão. Além de garantir
                que a população tenha acesso às iniciativas,  através do site abrimos espaço para que outras iniciativas se inscrevam
                para cadastramento e, também, para que voluntários se candidatem tanto para contribuir com o Salvador Pela Educação
                quanto para  ajudar a iniciativa que mais o atrai. O mapa será o principal meio de conexão com o cidadão e as iniciativas,
                um verdadeiro lar para os apaixonados por educação na cidade.
              </p>
              <p align="justify">Mais do que um portal de divulgação, o Mapeamento de Iniciativas Educacionais é uma tecnologia social para
                levar à população o conhecimento sobre o movimento educacional em nosso território.
              </p>
              <p align="justify">Acesse o mapa <a href="http://salvadorpelaeducacao.com.br/map">aqui</a>:</p>
            </div>
        </div>

        <div style="display:none;" id="tela-2" class="telas-menu col-xs-12 col-md-12" >
          <h1 class="topicos-sobre">TEDxRioVermelho</h1>
          <div class="col-xs-12 col-md-8">
            <p align="justify">
              O TED é uma organização sem fins lucrativos que teve início há 32 anos na Califórnia e tem crescido para apoiar
              ideias que mudam o mundo através de múltiplas iniciativas. Em uma conferência TED, pensadores e realizadores de
              todo o mundo são convidados a fazer a melhor palestra de suas vidas em 18 minutos ou menos. Entre os palestrantes
              que já participaram do TED estão Bill Gates, Elizabeth Gilbert, Benoit Mandelbrot, Philippe Starck e o
              ex-primeiro-ministro britânico Gordon Brown.
            </p>
          </div>
          <img src="/images/acao/tedxriovermelho-action.png" class="img_descricao_ted" alt="TedxRioVermelho" width="270" height="250">
          <div class="col-xs-12 col-md-12">
            <p align="justify">O TEDx é um evento independente onde o “x” significa “evento TED organizado de forma independente”. Ou seja, o
              TED Conferences provê orientações de como fazer este evento, mas sua organização é por meio de aplicações dos usuários
              do mundo com a concessão da licença de uso da marca e sem o auxílio do TED na montagem e criação da logística específica
              para o evento em si.
            </p>
            <p align="justify">A organização do TEDxRioVermelho é inteiramente voltada para a construção de um evento em que a Educação é
              protagonista. Na primeira edição utilizamos o tema “Conexões” para guiar todo o TEDxRioVermelho com a intenção de
              promover debates onde as palestras fossem o principal viés de conexão entre os participantes. O cerne da ideia é ser
              uma ponte que interligue pessoas desconhecidas através de temas desconhecidos e que uma aprenda com a outra e
              espalhe as novas ideias da conexão criada.
            </p>
            <p align="justify">Saiba mais <a href="https://tedxriovermelho.com/">aqui</a>:</p>
          </div>
        </div>

        <div style="display:none;" id="tela-3" class="telas-menu col-xs-12 col-md-12">
          <h1 class="topicos-sobre">Virada Educação Salvador</h1>
          <div class="col-xs-12 col-md-8">
            <p align="justify">A Virada da Educação é um movimento com o desafio de conectar espaços e pessoas para promover
              uma visão mais ampla sobre o aprender e o ensinar em diversos lugares e em muitos momentos do cotidiano. O
              desejo da mobilização surge da inquietação pela ausência de um evento que visibilize a educação em Salvador no
              seu sentido mais complexo e dinâmico.
            </p>
          </div>
          <img src="/images/acao/viradaeducacao.png" class="img_descricao_ted" alt="Virada Educação" width="270" height="250">
          <div class="col-xs-12 col-md-12">
            <p align="justify">A ideia principal é chamar a atenção da comunidade para o tema “Educação”, e torná-lo parte do
              dia a dia de todos os grupos sociais, promovendo o amplo acesso da população às discussões relacionadas ao tema.
            </p>
            <p align="justify">O público alvo é livre, já que  entendemos que a educação é para ser discutida, elaborada e
              difundida por todos. O evento acontece durante um dia inteiro em escolas, praças, centros culturais, etc.
              abrindo espaço para acontecer por todo o território em diferentes locais.
            </p>
            <p align="justify">As ações são organizadas em quatro categorias:</p>
            <ul>
              <li>Diálogos: Rodas de conversas para aproximar a escuta sobre questões essenciais da educação e nossas relações.</li>
              <li>Exibições: De filmes, peças teatrais, mostra de fotografias, música e muito mais.</li>
              <li>Intervenções:Ações que modificam o espaço, as pessoas e criam um movimento próprio. Exemplos: Grafites, esculturas, protestos, flash mob, mobilizações urbanas.</li>
              <li>Trilhas e oficinas: Atividades que promovem o aprendizado por meio de situações práticas que se conectam e exploram o território. Exemplos: Caminhadas, passeios, vivências na rua, etc.</li>
            </ul><br>
            <p align="justify">O movimento se desenvolve no sentido de empoderar crianças e profissionais da educação a co-criarem
              o processo de aprendizagem coletivamente e, ainda, criar uma rede de pessoas motivadas e com vontade de participar
              ativamente  do desenvolvimento de atividades direcionadas à educação.</p>
            <p align="justify">Saiba mais <a href="http://viradaeducacaossa.wixsite.com/2016">aqui</a>:</p>
          </div>
        </div>

        <div style="display:none;" id="tela-4" class="telas-menu col-xs-12 col-md-12">
          <h1 class="topicos-sobre">Saiba Mais</h1>
          <p align="justify">Muito além de realizar as açõesh citadas acima, que se propõem a construir as pontes entre
            iniciativas e pessoas, o nosso trabalho é contínuo para facilitar que todos os recursos (financeiros, humanos,
            material, de infraestrutura, etc) possam chegar às iniciativas e potencializar suas ações. Estaremos contribuindo
            a todo instante. Seja informalmente ao falar de uma iniciativa para outra, apresentando pessoas por email, durante
            os eventos, divulgando nas mídias sociais e inclusive nas entrevistas de rádio e TV. O Salvador Pela Educacão
            almeja que estas conexões, de fato, contribuam para garantir uma educacão de excelência com equidade em nossa
            cidade.
          </p>
        </div>
    </main>
</div>
@endsection
