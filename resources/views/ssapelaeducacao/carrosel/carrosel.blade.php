@extends('templates.carrosel') @section('main')
<h2>Carrosel</h2>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Descricao</th>
            <th>Imagem</th>
            <th>Ação</th>
        </tr>
    </thead>
    <tbody> @foreach ($carrosel as $carrosels)
        <tr>
            <td>{{$carrosels->titulo}}</td>
            <td> {{$carrosels->descricao}}</td>
            <td><img width="60" src="images/carrosel/{{$carrosels->avatar}}" alt="" /></td>
            <td>
                <a href="{{ url('/carrosel_edit', ['id'=>$carrosels->id]) }}">
                    <button id="btnEdit" class="btn btn-lg btn-warning fa fa-pencil" accesskey="a" hidden>
                        <br><u>A</u>lterar</button>
                </a>
                <a href="{{ url('/carrosel_destroy', ['id'=>$carrosels->id]) }}">
                    <button id="btnExc" class="btn btn-lg btn-danger fa fa-trash" accesskey="x" hidden>
                        <br>E<u>x</u>cluir</button>
                </a>
            </td>
        </tr> @endforeach </tbody>
</table> @endsection