@extends('layouts.app')
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Adicionar Imagem - Carrosel</h2>
            <form id="form-add" enctype="multipart/form-data" action="/carrosel_request" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="usr">Titulo:</label>
                    <input type="text" class="form-control" id="titulo" name="titulo"> </div>
                <div class="form-group">
                    <label for="txtEndereco">Descricao:</label>
                    <!-- <input type="text" class="form-control" id="end" name="end"> -->
                    <textarea class="form-control" rows="5" id="descricao" name="descricao"></textarea>
                </div>
                <br>
                <input type="file" name="image" />
                <br>
                <button type="submit" value="Enviar" name="btnEnviar" class="btn btn-primary">Enviar</button>
            </form>
        </div>
    </div>
</div> 

<script type="text/javascript" >
    window.onload = function()  {
        CKEDITOR.replace( 'descricao' );
    };
</script>
@endsection