<!-- @extends('templates.ssatemplate') -->@section('extend')
<li><a href="/carrosel">Carrosel</a></li>
<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li> @endsection @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Adicionar Imagem - Carrosel</h2>
            <form id="form-add" enctype="multipart/form-data" action="/carrosel_request" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="usr">Titulo:</label>
                    <input type="text" class="form-control" id="titulo" name="titulo"> </div>
                <div class="form-group">
                    <label for="txtEndereco">Descricao:</label>
                    <!-- <input type="text" class="form-control" id="end" name="end"> -->
                    <input type="text" class="form-control" id="descricao" name="descricao" /> </div>
                <br>
                <input type="file" name="image" />
                <br>
                <button type="submit" value="Enviar" name="btnEnviar" class="btn btn-primary">Enviar</button>
            </form>
        </div>
    </div>
</div> @endsection