<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Salvador Pela Educação</title>
	<link rel="shortcut icon" href="/images/site/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="/css/stylee.css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/navbar/navbar.css">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="/css/bootstrap.min.css" />
 	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" type='text/css'>
	<link href="/fonts/font-awesome.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
	@yield('content-script')
	@yield('map-script')
</head>
<body >
	<div class="tudo">
	<!-- Luan Menezes-->
    <nav class="navbar navbar-default navbar-fixed-top menu-topo">
		<div class="container">
			<img id="logo-site" href="#"src="/images/site/logo-nome-azul.png" class="logo-menu" />
			<div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle drop-menu" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			    </div>

					<div class="collapse navbar-collapse navbar-right" id="myNavbar">
			      <ul id="float-menu" class="navebar nav navbar-nav opcoes-menu menu-principal">
			        <li class="opcoes-menu"><a href="/">Home</a></li>
							<li class="opcoes-menu"><a href="/about">Sobre</a></li>
			        <li class="opcoes-menu"><a href="/actions">Ações</a></li>
			        <li class="opcoes-menu"><a href="/map">Mapa</a></li>
			        <li class="opcoes-menu"><a href="/whoweare">Quem Somos</a></li>
							<li class="opcoes-menu"><a href="/contact">Contato</a></li>
							<li><a href="https://www.facebook.com/salvadorpelaeducacao"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							@yield('extend')
			      </ul>
			    </div>
			</div>
		</div>
	</nav>
		@yield('carousel')
		<div id="topo" class="col-md-12 col-sm-12"></div>

		@yield('map')
		<div class="container-fluid">

				@yield('content')

		</div>





	<script src="/js/bootstrap.min.js"></script>
	<br>
	<br>
	<br>
	<br>
	<div class="navbar navbar-default navbar-fixed-bottom footer_spe_image">
     <div class="container footer_spe">
       <p class="navbar-text pull-left">© 2016 - Salvador Pela Educação
       </p>
       <!-- <a href="http://youtu.be/zJahlKPCL9g" class="navbar-btn btn-danger btn pull-right"> -->
     </div>


   </div>
	@yield('footer-script')
</body>
</html>
