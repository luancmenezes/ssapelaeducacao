<?php

use Illuminate\Database\Seeder;

class SetorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $setor = [
              0 =>[ 'descricao' => 'publico'],
              1 =>[ 'descricao' => 'privado'],
              2 =>[ 'descricao' => 'terceiro']
          ];
      DB::table('setor')->insert($setor);
    }
}
