<?php

use Illuminate\Database\Seeder;

class IniciativasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('iniciativa')->insert();
    }
}
