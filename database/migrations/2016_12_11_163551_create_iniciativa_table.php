<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIniciativaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema::create('iniciativa', function(Blueprint $table) {
          $table->increments('id');
          $table->string('nome');
          $table->string('endereco');
          $table->text('email');
          $table->string('avatar')->default('default.jpg');
          $table->text('site');
          $table->text('facebook');
          $table->text('contato');
          $table->text('organizadores');
          $table->text('alunos');
          $table->text('tempo_existencia');
          $table->text('custo');
          $table->text('alcance');
          $table->text('tipo_atividade');
          $table->text('atuacao_iniciativa');
          $table->text('definicao');
          $table->text('motivo');
          $table->text('metodo');
          $table->text('finalidade');
          $table->text('resultado');
          $table->boolean('ativo');
          $table->float('latitude',20,15);
          $table->float('longitude',20,15);
          $table->string('indice_setor');
          $table->string('indice_nivel_educacao');
          $table->string('indice_forma_educacao');
          $table->text('requisitos_participar');
          $table->text('resume');
          $table->text('demandas');
          $table->text('contribuicao');
        //  $table->foreign('indice_setor')->references('id')->on('setor');
        //  $table->foreign('indice_nivel_educacao')->references('id')->on('niveleducacao');
        //  $table->foreign('indice_forma_educacao')->references('id')->on('formaeducacao');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('iniciativa');
    }
}
