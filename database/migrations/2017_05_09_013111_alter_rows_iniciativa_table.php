<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRowsIniciativaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('iniciativa', function (Blueprint $table) {
            $table->dropColumn(['motivo','metodo','finalidade']);
            $table->text('atividades_desenvolvidas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('iniciativa', function (Blueprint $table) {
          $table->text('motivo');
          $table->text('metodo');
          $table->text('finalidade');
        });
    }
}
