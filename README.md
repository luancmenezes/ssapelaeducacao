# Salvador Pela Educação 
===================
### O que é?

O **Salvador Pela Educação** é um movimento criado para construir pontes entre iniciativas, recursos e pessoas no campo da **EDUCAÇÃO**. Acreditamos que facilitar o diálogo e criar conexões entre pessoas que trabalham ou se interessam por educação na cidade potencializa as ações educacionais do município e, consequentemente, melhora a qualidade da educação local. Nossos principais objetivos são:

-  Chamar atenção para o tema Educação no município de Salvador.
-  Conectar os cidadãos soteropolitanos às iniciativas educacionais da cidade.
-  Articular as iniciativas de educação entre si para potencializar suas ações."

## "Nosso conceito de educação"

### Conteúdo:

"As iniciativas educacionais que estão contempladas pelo nosso conceito de educação são as mais diversas possíveis. Buscamos atingir todas as faixas etárias (da creche ao pós-doutorado) e adotamos o conceito que classifica a educação em 3 tipos:

- Formal: acontece nas instituições educativas que seguem um currículo regido pela legislação, como por exemplo escolas, universidades e institutos federais.

- Não formal: acontece em locais que não necessitam seguir o currículo legal, como por exemplo museus, centros culturais e galerias.

- Informal:acontece em locais não predestinados à educação, desde discussões entre amigos no restaurante até cortejos nas praças e movimentos sociais pelas ruas.

Para nós, o processo educativo se dá dentro do educando e em sua relação com o mediador, que pode ser um professor, um livro ou uma máquina. Educar, partindo de uma de suas raízes etimológicas, significa extrair, ou seja, apresentar ao mundo as habilidades que o indivíduo construiu. É trazer algo novo ao ser humano. É lhe dar a oportunidade de ser alguém melhor para si e para o mundo. É por isso que, para além de qualquer outro significado, o Salvador pela Educação entende que educar é um ato de amor."
