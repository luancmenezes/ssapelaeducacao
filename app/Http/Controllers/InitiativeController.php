<?php

namespace App\Http\Controllers;

use Auth;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Iniciativa;
use App\Http\Requests;



class InitiativeController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
    }

  public function initiativeShow()
  {
      $initiative = Iniciativa::all();
      return view('ssapelaeducacao.initiative.initiative_list', ['initiative' => $initiative]);
  }
  public function initiative()
  {
    return view('ssapelaeducacao.initiative.initiative');
  }
  public function initiative_add(){
    return view('ssapelaeducacao.addproject');
  }
  public function initiative_edit($id)
  {
    $initiative = DB::table('iniciativa')->where('id',$id)->first();
    return view( 'ssapelaeducacao.initiative.initiative_edit',compact('initiative'));
  }

  public function edit(Request $request){
      $id = $request->id;
      if (iniciativa::find($id))
      {
        $destinationPath = 'images/projetos/'; // upload path
        //old_image
        $old_image_id = iniciativa::find($id)->avatar;

        //new_image
        $rules = array('image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000');
        $file = array('image' => Input::file('image'));
        $validator = Validator::make($file, $rules);

        if ($request->file('image')->isValid())
        {
          $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
          $fileName = time().'.'.$extension; // renameing image
          // $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
          $newupload =Input::file('image')->move($destinationPath, $fileName);
        }
        else
        {
          $message = 'Upload não e válido';
          Log::alert($message);
        }
        if($old_image_id != $fileName ){
          // dd("rm /home/lmenezes/Documents/ssapelaeducacao/public/images/projetos/".$old_image_id);
          shell_exec("rm /home/lmenezes/Documents/ssapelaeducacao/public/images/projetos/".$old_image_id);
          // shell_exec("rm ../../../public/images/projetos/"+$image_id);
        }
      }

      $data = [
       'nome'=>$request->nome
      ,'endereco'=>$request->endereco
      ,'email'=>$request->email
      ,'site'=>$request->site
      ,'facebook'=>$request->facebook
      ,'contato'=>$request->contato
      ,'organizadores'=>$request->organizadores
      ,'alunos'=>$request->alunos
      ,'tempo_existencia'=>$request->tempo_existencia
      ,'custo'=>$request->custo
      ,'alcance'=>$request->alcance
      ,'tipo_atividade'=>$request->tipo_atividade
      ,'atuacao_iniciativa'=>$request->atuacao_iniciativa
      ,'definicao'=>$request->definicao
      ,'atividades_desenvolvidas'=>$request->atividades
      ,'resultado'=>$request->resultado
      ,'indice_setor'=>$request->indice_setor
      ,'indice_nivel_educacao'=>$request->indice_nivel_educacao
      ,'indice_forma_educacao'=>$request->indice_forma_educacao
      ,'requisitos_participar'=>$request->requisitos_participar
      ,'resume'=>$request->resume
      ,'demandas'=>$request->demandas
      ,'contribuicao'=>$request->contribuicao
      ,'avatar' => $fileName
      ,'id'=>$request->id];

      DB::table('iniciativa')->where('id',$request->id)->update($data);
      return Redirect('initiative_list');

  }
  public function initiative_destroy($id)
  {
    if (iniciativa::find($id)) {
      $image_id = iniciativa::find($id)->avatar;
      iniciativa::find($id)->delete();
      shell_exec("rm ../../../public/images/projetos/".$image_id);
    }
    $initiative = Iniciativa::all();
    return view('ssapelaeducacao.initiative.initiative_list', ['initiative' => $initiative]);
  }
}
