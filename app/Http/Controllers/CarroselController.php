<?php

namespace App\Http\Controllers;

use Auth;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Carrosel;

class CarroselController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function carrosel_list()
  {
      $carrosel = Carrosel::all();
      return view('ssapelaeducacao.carrosel.carrosel_list', ['carrosel' => $carrosel]);
  }
  public function carrosel_create()
  {
    return view('ssapelaeducacao.carrosel.carrosel_create');
  }
  public function carrosel_request(Request $request)
  {
        $titulo = $request->input('titulo');
        $descricao = $request->input('descricao');
        $file = array('image' => Input::file('image'));
        $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($request->file('image')->isValid())
        {
          $destinationPath = 'images/carrosel/'; // upload path
          $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
          $fileName = time().'.'.$extension; // renameing image
          $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
        }
        else
        {
          $message = 'Upload não e válido';
          Log::alert($message);
        }
        DB::insert
        ('insert into carrosel (titulo,descricao,avatar)
        values (?,?,?)',
        array($titulo,$descricao,$fileName));

        return Redirect('carrosel');
    }
    public function carrosel_update($id)
    {

      $input = Input::all();
      $carrosel = Carrosel::find($id);
      $carrosel->update($input);

      $carrosel = Carrosel::all();
      return view('ssapelaeducacao.carrosel.carrosel', ['carrosel' => $carrosel]);
    }

    public function carrosel_edit($id)
    {
        $carrosel = Carrosel::where('id',$id)
                 ->get();
      return view( 'ssapelaeducacao.carrosel.carrosel_edit')->with('carrosel',$carrosel);
    }
  public function carrosel_destroy($id)
  {
    Carrosel::find($id)->delete();
    $carrosel = Carrosel::all();
    return Redirect('carrosel');
  }

}
