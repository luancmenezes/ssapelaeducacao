<?php
namespace App\Http\Controllers;

use Auth;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ContatoRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Carrosel;
class SalvadorController extends Controller
{


  public function index(Request $request){
     $nome = $request->input('nome');
     $qtd = DB::table('iniciativa')->count();
     $ctd_ini = 0;
     $results = DB::table('iniciativa')->where('nome', $nome)->get();
     $iniciativa = DB::table('iniciativa')->orderBy('nome')->paginate(9);
     $carrosels = Carrosel::all();
     return view('ssapelaeducacao.home')->with('results',$results)->with('iniciativa',$iniciativa)->with('qtd',$qtd)->with('carrosels',$carrosels)->with('ctd_ini',$ctd_ini);

}

  public function index2(Request $request){

  $request->all();
  dd($resquest);
}
  public function about(){
    return view('ssapelaeducacao.about');
  }
  public function map(){
    $iniciativa = DB::select('select * from iniciativa');
    return view('ssapelaeducacao.map')->with('iniciativa',$iniciativa);
}
  public function actions(){
    return view('ssapelaeducacao.actions');
}
  public function contact(){
    return view('ssapelaeducacao.contact');
}
  public function whoweare(){
    return view('ssapelaeducacao.whoweare');
}

  public function initiative($id){
    $iniciativa = DB::table('iniciativa')->where('id',$id)->get();
    //  dd($iniciativa);
    $setor = DB::table('setor')
            ->join('iniciativa', 'setor.id', '=', 'iniciativa.indice_setor')
            ->select('setor.descricao')
            ->get();
    $nivel = DB::table('niveleducacao')
            ->join('iniciativa', 'niveleducacao.id', '=', 'iniciativa.indice_nivel_educacao')
            ->select('niveleducacao.descricao')
            ->get();

    $forma = DB::table('formaeducacao')
            ->join('iniciativa', 'formaeducacao.id', '=', 'iniciativa.indice_forma_educacao')
            ->select('formaeducacao.descricao')
            ->get();

    return view('ssapelaeducacao.initiative')->with('iniciativa',$iniciativa);
  }


  public function adiciona(Request $request){

      $nome = $request->input('nome');
      $endereco = $request->input('end');
      $oque = $request->input('oque');
      $atividades_desenvolvidas = $request->input('atividades');
      $resultados = $request->input('resultados');
      $tempo = $request->input('tempo');
      $custo = $request->input('custo');
      $alcance = $request->input('alcance');
      $bool = 1;
      $latitude = $request->input('txtLatitude');
      $longitude = $request->input('txtLongitude');
      $setor = $request->input('setor');
      $nivel = $request->input('nivel');
      $forma= $request->input('forma');
      $email = $request->input('email');
      $site = $request->input('site');
      $facebook = $request->input('facebook');
      $contato = $request->input('contato');
      $organizadores = $request->input('organizadores');
      $alunos = $request->input('alunos');
      $atuacao_iniciativa = $request->input('atuacao_iniciativa');
      $tipo_atividade = $request->input('tipo_atividade');
      $requisitos_participar = $request->input('requisitos_participar');
      $resume = $request->input('resume');
      $demandas = $request->input('demandas');
      $contribuicao = $request->input('contribuicao');
      $file = array('image' => Input::file('image'));
      $rules = array('image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',); //mimes:jpeg,bmp,png and for max size max:10000
      // doing the validation, passing post data, rules and the messages
      $validator = Validator::make($file, $rules);
      if ($request->file('image')->isValid())
      {
        $destinationPath = 'images/projetos/'; // upload path
        $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
        $fileName = time().'.'.$extension; // renameing image
        $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
      }
      else
      {
        $message = 'Upload não e válido';
        Log::alert($message);
      }
      DB::insert
      ('insert into iniciativa (nome, endereco, definicao,atividades_desenvolvidas, resultado, tempo_existencia, custo, alcance, ativo,latitude,
       longitude,indice_setor,indice_nivel_educacao,indice_forma_educacao,avatar,
       email,site,facebook,contato,organizadores,alunos,atuacao_iniciativa,tipo_atividade,requisitos_participar,resume,demandas,contribuicao)
       values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
      array(
        $nome, $endereco, $oque,$atividades_desenvolvidas, $resultados,$tempo,
        $custo,$alcance,$bool,$latitude,$longitude,$setor,$nivel,$forma,$fileName,
        $email,$site,$facebook,$contato,$organizadores,$alunos, $atuacao_iniciativa,
        $tipo_atividade,$requisitos_participar,$resume,$demandas,$contribuicao));

    return view('ssapelaeducacao.addproject');
  }
  public function email(){
      $data = [
        'name'=>Request::input('nome'),
        'email'=>Request::input('email'),
        'mensagem'=>Request::input('mensagem')
      ];

      Mail::send('ssapelaeducacao.mail', $data, function($message) {
         $message->to('luancmenezes@gmail.com', 'Tutorials Point')->subject
            ('Contato Email');
         $message->from('luancmenezes@gmail.com','Virat Gandhi');
      });

      \Session::flash('flash_message',' A Mensagem foi enviada, retornaremos em breve.');

      return back();
   }
  //  public function getLogin(){
  //   return view('auth.login');
  //  }

}
