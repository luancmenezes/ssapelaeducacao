<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Mail;

Route::get('sendbasicemail','MailController@basic_email');
Route::get('sendattachmentemail','MailController@attachment_email');
Route::get('home', 'HomeController@index');
Route::get('/', 'SalvadorController@index');
Route::get('about', 'SalvadorController@about');
Route::get('map', 'SalvadorController@map');
Route::get('actions', 'SalvadorController@actions');
Route::get('whoweare', 'SalvadorController@whoweare');
Route::get('initiative/{id}', 'SalvadorController@initiative');
Route::get('contact', 'SalvadorController@contact');
Route::post('adiciona', 'SalvadorController@adiciona');
Route::post('email', 'SalvadorController@email');
Route::auth();
Route::get('carrosel', 'CarroselController@carrosel_list');
Route::get('carrosel_create', 'CarroselController@carrosel_create');
Route::post('carrosel_request', 'CarroselController@carrosel_request');
Route::get('carrosel_edit/{id}','CarroselController@carrosel_edit');
Route::post('carrosel_update/{id}','CarroselController@carrosel_update');
Route::get('carrosel_destroy/{id}','CarroselController@carrosel_destroy');
Route::get('initiative_add','InitiativeController@initiative_add');
Route::get('initiative_edit/{id}','InitiativeController@initiative_edit');
Route::get('initiative_list','InitiativeController@initiativeShow');
Route::post('edit','InitiativeController@edit');
Route::get('initiative_destroy/{id}','InitiativeController@initiative_destroy');
Route::get('initiative_edit','InitiativeController@initiative');
