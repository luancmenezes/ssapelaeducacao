@extends('templates.ssatemplate')
@section('content-script')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/mapa.js"></script>
<script type="text/javascript" src="js/jquery-ui.custom.min.js"></script>
@endsection


@section('content')
<div id="apresentacao">

    <h1>Google Maps API v3: Busca de endereço e Autocomplete - Demo</h1>

    <form method="post" action="index.html">
        <fieldset>

            <legend>Google Maps API v3: Busca de endereço e Autocomplete - Demo</legend>

            <div class="campos">
                <label for="txtEndereco">Endereço:</label>
                <input type="text" id="txtEndereco" name="txtEndereco" />
                <input type="button" id="btnEndereco" name="btnEndereco" value="Mostrar no mapa" />
            </div>

            <div id="mapa"></div>

          <input type="submit" value="Enviar" name="btnEnviar" />

            <input type="hidden" id="txtLatitude" name="txtLatitude" />
            <input type="hidden" id="txtLongitude" name="txtLongitude" />

        </fieldset>
    </form>


</div>


@endsection
