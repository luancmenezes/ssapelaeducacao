$(document).ready(function(){

  // store filter for each group
   var filters = {};
   $('.filters').on('click', '.button', function () {
       var $this = $(this);
    //   get group key
       var $buttonGroup = $this.parents('.button-group');
       var filterGroup = $buttonGroup.attr('data-filter-group');
    //   set filterfor group
       filters[filterGroup] = $this.attr('data-filter');
     //  console.log(filters[filterGroup]);
    //   combine filters
       var filterValue = concatValues(filters);
       console.log(filterValue);
     //  set filter for Isotope

   });
 //  change is - checked class on buttons
   $('.button-group').each(function (i, buttonGroup) {
       var $buttonGroup = $(buttonGroup);
       $buttonGroup.on('click', 'button', function () {
           $buttonGroup.find('.is-checked').removeClass('is-checked');
           $(this).addClass('is-checked');
       });
   });

   //access Isotope properties console.log(elems) flatten object by concatting values
   function concatValues(obj) {
       var value = '';
       for (var prop in obj) {
           value += obj[prop];
       }
       return value;
   }


   var id = 12;  
   $.ajax({
       method: 'POST', // Type of response and matches what we said in the route
       url: '/', // This is the url we gave in the route
       data: {'id' : id}, // a JSON object to send back
       success: function(response){ // What to do if we succeed
           console.log(response);
       },
       error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
           console.log(JSON.stringify(jqXHR));
           console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
       }
   });

});
