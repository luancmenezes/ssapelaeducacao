<?php $__env->startSection('content'); ?>
<div class="container">
<div class="row">
  <div class="contact">
  <div class="col-md-6 col-sm-6">
    <h1>Inscrições</h1>
    <br /><br />
   <h3>Voluntários</h3>
   <div class="">
     <br>
     <a class="embedly-card" href="https://salvadorpelaeducacao.typeform.com/to/tbB2Yg">Inscrição para voluntários</a>
   </div>
   <br /><br />
   <h3>Projetos</h3>
   </div>
  </div>
  <div class="col-md-6 col-sm-6">
    <!-- <?php $mensagem='2' ?>
    <?php if($mensagem=='1'): ?>
    <div class="alert alert-success">
    <strong>Sucesso!</strong> A Mensagem foi enviada, retornaremos em breve.
    </div>
    <?php endif; ?> -->
    <?php if(Session::has('flash_message')): ?>
    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> <?php echo session('flash_message'); ?></em></div>
    <?php endif; ?>
    <div class="contact">

      <h1>Fale Conosco</h1>

    <hr />
    <?php if ( ! ($errors->isEmpty())): ?>
    <div class="alert alert-danger">
    <ul>
    <?php foreach($errors->getMessages() as $error): ?>
        <li><?php echo e($error[0]); ?></li>
    <?php endforeach; ?>
    </ul>
    <?php endif; ?>
    </div>
    <form action="/email" method="post">
      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

      <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" required>
      </div>

      <div class="form-group">
        <label for="email">E-Mail</label>
        <input type="text" id="email" name="email" class="form-control" placeholder="E-Mail" required>
      </div>

      <div class="form-group">
        <textarea id="mensagem" name="mensagem" class="form-control" placeholder="Digite sua mensagem" required></textarea>
      </div>

      <button type="submit" class="btn btn-primary">Enviar</button>

    </form>
    </div>
  </div>
</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('templates.ssatemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>