<?php $__env->startSection('extend'); ?>
	<li><a href="/carrosel">Carrosel</a></li>
<li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content-script'); ?>

<script type="text/javascript" src="/js/mapa.js"></script>
<script type="text/javascript" src="/js/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDJOgSlPsYaplvGq7aU8qRgatgBB2Tk-FA"></script>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
  <div class="container">
    <div class="row">
      <div  class="col-md-12">
      <h2>Adicionar Projeto</h2>
        <form id="form-add" enctype="multipart/form-data" action="/adiciona" method="post" >

          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
            <div class="form-group">
                <label for="usr">Nome da Iniciativa:</label>
                <input type="text" class="form-control" id="nome" name="nome">
              </div>
              <div class="form-group">
                <label for="txtEndereco">Endereço:</label>
                <!-- <input type="text" class="form-control" id="end" name="end"> -->
                <input type="text" class="form-control" id="txtEndereco" name="end" />
                <input type="button" id="btnEndereco" name="btnEndereco" value="Mostrar no mapa" />
              </div>
              <div id="mapa" style="height: 300px; width: 300px"></div>
              <br>
              <input type="file" name="image" />
              <br>
              <div class="form-group">
                <label for="usr">Email:</label>
                <input type="text" class="form-control" id="email" name="email">
              </div>
              <div class="form-group">
                <label for="usr">Site:</label>
                <input type="text" class="form-control" id="site" name="site">
              </div>
              <div class="form-group">
                <label for="usr">Perfil Facebook:</label>
                <input type="text" class="form-control" id="facebook" name="facebook">
              </div>
              <div class="form-group">
                <label for="usr">Contato (Telefone / Whatsapp):</label>
                <input type="text" class="form-control" id="contato" name="contato">
              </div>
              <div class="form-group">
                <label for="usr">Número atual de organizadores:</label>
                <input type="text" class="form-control" id="organizadores" name="organizadores">
              </div>
              <div class="form-group">
                <label for="usr">Número atual de alunos:</label>
                <input type="text" class="form-control" id="alunos" name="alunos">
              </div>
              <div class="form-group">
                <label for="usr">Tempo de Existência:</label>
                <input type="text" class="form-control" id="tempo" name="tempo">
              </div>
              <div class="form-group">
                <label for="usr">Custo da Iniciativa:</label>
                <input type="text" class="form-control" id="custo" name="custo">
              </div>
              <div class="form-group">
                <label for="usr">Alcance:</label>
                <input type="text" class="form-control" id="alcance" name="alcance">
              </div>
              <div class="form-group">
                <label for="comment">Tipo de atividade:</label>
                <br />
                  <select name="tipo_atividade" class="c-select">
                    <option selected>Abra as opções</option>
                    <option value="cultura" >Cultura</option>
                    <option value="formal">Ensino Formal</option>
                    <option value="esporte">Esporte</option>
                    <option value="outros">Outros</option>
                </select>
              </div>
              <div class="form-group">
                <label for="comment">Atuação da inciativa:</label>
                <br />
                  <select name="atuacao_iniciativa" class="c-select">
                    <option selected>Abra as opções</option>
                    <option value="comunidade">Comunidade</option>
                    <option value="bairro">Bairro</option>
                    <option value="cidade">Cidades</option>
                    <option value="outros">Outros</option>
                </select>
              </div>
              <div class="form-group">
                <label for="comment">O que ?</label>
                <textarea class="form-control" rows="5" id="oque" name="oque"></textarea>
              </div>
              <div class="form-group">
                <label for="comment">Por que ?</label>
                <textarea class="form-control" rows="5" id="porque" name="porque"></textarea>
              </div>

              <div class="form-group">
                <label for="comment">Como ?</label>
                <textarea class="form-control" rows="5" id="como" name="como"></textarea>
              </div>
              <div class="form-group">
                <label for="comment">Para que?</label>
                <textarea class="form-control" rows="5" id="paraque" name="paraque"></textarea>
              </div>
              <div class="form-group">
                <label for="comment">Principais Resultados:</label>
                <textarea class="form-control" rows="5" id="resultados" name="resultados"></textarea>
              </div>
              <div class="form-group">
                <label for="comment">Setor:</label>
                <br />
                  <select name="setor" class="c-select">
                    <option selected>Abra as opções</option>
                    <option value="publico" >Público</option>
                    <option value="privado">Privado</option>
                    <option value="terceirosetor">Terceiro Setor</option>
                </select>
              </div>

              <div class="form-group">
                <label   for="comment">Forma de Educação:</label>
                <br />
                <select name="forma" class="c-select">
                    <option selected>Abra as opções</option>
                    <option value="formal">Formal</option>
                    <option value="naoformal">Não Formal</option>
                    <option value="informal">Informal</option>
                </select>
              </div>
              <div class="form-group">
                <label name="formaeducacao" for="comment">Nível de Educação:</label>
                <br />
                <select name="nivel" class="c-select">
                    <option selected>Abra as opções</option>
                    <option value="infantil">Infantil</option>
                    <option value="fundamental">Fundamental</option>
                    <option value="medio">Médio</option>
                    <option value="profissional">Profissional</option>
                    <option value="superior">Superior</option>
                </select>
              </div>
              <div class="form-group">
                <label for="comment">Requisitos para participar:</label>
                <textarea class="form-control" rows="5" id="resquitos_participar" name="resquitos_participar"></textarea>
              </div>
              <div class="form-group">
                <label for="comment">Principais demandas da iniciativa:</label>
                <textarea class="form-control" rows="5" id="demandas" name="demandas"></textarea>
              </div>
              <div class="form-group">
                <label for="comment">Contribuição da iniciativa com o SPE:</label>
                <textarea class="form-control" rows="5" id="contribuicao" name="contribuicao"></textarea>
              </div>


              <button type="submit" value="Enviar" name="btnEnviar" class="btn btn-primary">Enviar</button>
              <input type="hidden" id="txtLatitude" name="txtLatitude" />
              <input type="hidden" id="txtLongitude" name="txtLongitude" />
      </form>
    </div>
  </div>
</div>





<?php $__env->stopSection(); ?>

<?php echo $__env->make('templates.ssatemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>