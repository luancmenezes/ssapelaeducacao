
<html>
<head>
  <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Salvador Pela Educação</title>
	<link rel="shortcut icon" href="/images/site/favicon.png">
  <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>/ -->
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHGnuvOE8PeZM6qza7IwH7uN2IpBdDfcY&callback=initMap"
  type="text/javascript"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/gmaps.js"></script>
  <link rel="stylesheet" type="text/css" href="css/map.css" />
  <link rel="stylesheet" type="text/css" href="css/app.css" />
  <!-- Fonts -->
   <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" type='text/css'>



    <?php
    $latitude = array();
    $longitude = array();
    $titulo = array();
    $avatar = array();

    foreach ($iniciativa as $ini){
      array_push($latitude, "$ini->latitude");
      array_push($longitude, "$ini->longitude");
      array_push($titulo,"$ini->nome");
      array_push($avatar, "$ini->avatar");
    }
     ?>

  <script type="text/javascript">
    var map;
    var i=0;

    iniciativa = <?php echo json_encode($iniciativa); ?>;
    var lat = <?php echo json_encode($latitude); ?>;
    var long = <?php echo json_encode($longitude); ?>;
    var coordinates = [];

    var Coordinate = function(lat, lng) {
        this.lat = lat;
        this.lng = lng;
      }

    for (var i = 0; i < lat.length; i++) {
          coordinates.push(new Coordinate(lat[i],long[i]));
      }

    function addPlace(map, coordinate,iniciativa,i) {
        console.log('tete');
        console.log(i);
        map.addMarker({
          lat: coordinate.lat,
          lng: coordinate.lng,

          title: 'Marker with InfoWindow',
          infoWindow: {
            content: '<div id="box-map" style="float:right;padding-left: 20px;"> \
            <h1 class="initiative-title">' + iniciativa[i].nome + '</h1> \
              </div>'
          },
          icon: "images/site/pin-blue.png"
        });
        i++;
      }

    var cc = [];
    $(document).ready(function(){
      map = new GMaps({
        el: '#map',
        lat: -12.994073,
        lng: -38.5028423
      });
      i=0;
      coordinates.forEach(function (coordinate) {
        addPlace(map, coordinate, iniciativa, i);
        i++;
      });
    });
  </script>
</head>
<body>
  <nav class="navbar navbar-default navbar-fixed-top">
	<img id="logo-site" href="#"src="/images/site/logo-nome-azul.png" width="180" height="69" />
<div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <!-- <a class="navbar-brand" href="#">WebSiteName</a> -->
  </div>
  <div class="collapse navbar-collapse" id="myNavbar">
    <ul id="float-menu" class="nav navbar-nav">
      <li class="active"><a href="/salvadorpelaeducacao">Home</a></li>
      <li><a href="about">Sobre</a></li>
      <li><a href="actions">Ações</a></li>
      <li><a href="map">Mapa</a></li>
      <li><a href="whoweare">Quem Somos</a></li>
      <li><a href="contact">Contato</a></li>
    </ul>
  </div>
</div>
</nav>
    <div class="span11">
      <div id="map"></div>
    </div>
</body>
</html>
