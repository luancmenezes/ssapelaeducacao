<?php $__env->startSection('content-script'); ?>
<script type="text/javascript" src="/js/modernizr.custom.79639.js"></script>
<link rel="stylesheet" type="text/css" href="/css/style6.css">
<link rel="stylesheet" type="text/css" href="/css/demo.css">
<link rel="stylesheet" type="text/css" href="/css/common.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div>
<div class="container">
<div class="row">
    <section class="col-md-6">
      <div class="description">
    <h1>Quem Somos</h1>
    <br />
    <br />
    <p>
      Cidadãs e cidadãos soteropolitanos das mais diversas áreas de atuação profissional
      (Educação, Economia, Administração, Direito, etc.) que se interessam pela melhoria da
      qualidade da educação na cidade. A equipe se conheceu na organização da Virada Educação 2015
      e foi lá que perceberam que tinham um propósito em comum: contribuir para a transformação da cidade
      por meio da educação. O grupo tem como uma de suas principais características o desejo constante de atuar
      ao lado das comunidades, onde a educação acontece em sua essência.
    </p>
    </div>
  </section>
    <section class="col-md-6">
      <div >
          <img src="/images/site/ssapelaeducacao.png" class="img-responsive img-circle" alt="Cinque Terre" width="304" height="236">
      </div>
    </section>


</div>
</div>
<br></br>
<br></br>
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <div class="members">
         <ul class="ch-grid">

            <li>
             <a style="cursor:pointer;"  id="person1">
              <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/1.jpg">
              </div>

              </a>
              <h3 class="h3color">Alan Carvalho</h3>
              <p style="font-size:12px;">Formado em Engenharia Mecatrônica</p>
            </li>

        </ul>
       
    

    <!-- The Modal -->
        <div id="myModal1" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2>Alan Carvalho</h2>
            </div>
            <div class="modal-body">
               <p class="litlebio biografia">Alan Carvalho, 25 anos, natural de Salvador, apaixonado por educação da
              cabeça aos pés. É formado em engenharia mecatrônica pela USP, fez cursos de psicologia e
              educação na CSU-Chico e estágio acadêmico no Centro Lemann em Stanford. Trabalhou com formação de
              professores na startup Neofuturo, como gestor de operações.
              No terceiro setor, foi gerente de projetos no LABi.
              </p>
            </div>
            
          </div>

        </div>



    </div>
    </div>
    <div class="col-md-4">
      <div class=" members">
         <ul class="ch-grid">

            <li>
             <a style="cursor:pointer;"  id="person2">
                <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/2.jpg">
              </div>

              </a>
              <h3 class="h3color">Angra Valesca</h3>
              <p style="font-size:12px;">Formanda em Psicologia</p>
            </li>

        </ul>
        
        
        <!-- Modal -->
        <div id="myModal2" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2>Angra Valesca</h2>
            </div>
            <div class="modal-body">
                <p class="litlebio">Angra Valesca, 23 anos, natural de São Paulo. Já realizou
             estágio em Recrutamento e Seleção pelo SESC Bahia, participou de projetos de
             educação para carreira; atualmente é formanda em Psicologia pela Universidade
             Federal da Bahia e professora de educação infantil.</p>
            </div>
            
          </div>

        </div>



    </div>
    </div>
    <div class="col-md-4">
      <div class="members">
        <ul class="ch-grid">

            <li>
             <a style="cursor:pointer;"  id="person3">
              <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/3.jpg">
              </div>

              </a>
              <h3 class="h3color">Cristiano Lima Nascimento</h3>
              <p style="font-size:12px;" class="cris">Estudante de Medicina</p>
            </li>

        </ul>
        
             <!-- Modal -->
        <div id="myModal3" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2 class="titulo-modal">Cristiano Lima Nascimento</h2>
            </div>
            <div class="modal-body">
               <p class="litlebio">Cristiano Lima Nascimento, 23 anos, natural de Salvador,
              estudante de Medicina na Escola Bahiana de Medicina e Saúde Pública, adepto
               de movimentos locais, nacionais e internacionais de capacitação, assistencialismo
                e auxílio à comunidades e impacto social. Presidente e fundador da Liga
                 Acadêmica de Psiquiatria (LAP), membro das organizações Rotaract, Global
                 Shapers e CISV International. </p>
            </div>
            
          </div>

        </div>     
        

    </div>
    </div>
    <div class="col-md-4">
      <div class="members">
        <ul class="ch-grid">

            <li>
              <a style="cursor:pointer;"  id="person4">
              <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/4.jpg">
              </div>

              </a>
              <h3 class="h3color">Fernanda Gusmão</h3>
              <p style="font-size:12px;">Estudante de Administração</p>
            </li>

        </ul>
        
           <!-- Modal -->
        <div id="myModal4" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2 class="titulo">Fernanda Gusmão</h2>
            </div>
            <div class="modal-body">
              <p class="litlebio">Fernanda Gusmão, 22 anos, estudante de Administração da Universidade
             Federal da Bahia, vice-presidente da ONG Creche Cantinho Encantado, Gerente de Projetos
             de intercâmbios sociais da AIESEC Salvador, apaixonada pelo Terceiro setor, educação,
             economia solidária e empreendedorismo.   </p>
            </div>
            
          </div>

        </div>     

    </div>
    </div>
    <div class="col-md-4">
    <div class="members">
     <ul class="ch-grid">

            <li>
             <a style="cursor:pointer;"  id="person5">
              <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/5.jpg">
              </div>

              </a>
              <h3 class="h3color">Henrique Souza Santos</h3>
              <p style="font-size:12px;">Estudante de Medicina</p>
            </li>

        </ul>
      <!-- Modal -->
        <div id="myModal5" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2 class="titulo-modal">Henrique Souza Santos</h2>
            </div>
            <div class="modal-body">
                      <p class="litlebio">Henrique Souza Santos, 21 anos, soteropolitano, estudante de Medicina na Escola Bahiana de Medicina e vislumbrado por iniciativas de cunho colaborativo. Membro fundador da Liga Acadêmica de Medicina Generalista e da Liga Acadêmica de Psiquiatria bem como membro da linha de pesquisa Comportamento e Aprendizado Motor.</p>

            </div>
            
          </div>

        </div>    


    </div>
    </div>
    <div class="col-md-4">
    <div class="members">
       <ul class="ch-grid">

            <li>
             <a style="cursor:pointer;"  id="person6">
              <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/6.jpg">
              </div>

              </a>
              <h3 class="h3color">Laíse Santos</h3>
              <p style="font-size:12px;">Estudante de Economia</p>
            </li>

        </ul>
        
     </ul>
      <!-- Modal -->
        <div id="myModal6" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2>Laíse Santos</h2>
            </div>
            <div class="modal-body">
             <p class="litlebio">Laíse Santos, 21 anos, radicada em Salvador, cursa Economia
              na UFBA. Trabalhou com Administração Financeira no setor privado e Gestão de
              Fundos no governo da Bahia.  Desde 2013, atua em Gestão de Projetos, tendo
              assumido cargos de liderança e alto impacto no Rotary International, além de
              compor o Global Shapers Hub Salvador, ser co-organizer e diretora de logística
              no TEDxRioVermelho e embaixadora do Ensina Brasil. Amante da cultura e
              apaixonada pelo mundo dos projetos, nas horas vagas desbrava as informações
              infinitas que o universo oferece. </p>
            </div>
            
          </div>

        </div>   

    </div>
    </div>
    </div>


   <div class="col-md-4">
    <div class="members">
      <ul class="ch-grid">

            <li>
             <a style="cursor:pointer;"  id="person7">
              <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/7.jpg">
              </div>

              </a>
              <h3 class="h3color">Matheus Caldas</h3>
              <p style="font-size:12px;">Formado em Administração</p>
            </li>

        </ul>

     <!-- Modal -->
        <div id="myModal7" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2>Matheus Caldas</h2>
            </div>
            <div class="modal-body">
                 <p class="litlebio">Matheus Caldas, 24 anos, natural de Salvador, formado em Administração pela Universidade do Estado da Bahia, um dos fundadores do movimento Salvador pela Educação, realiza trabalho voluntário pelo Rotaract, diretor de programação do TEDxRioVermelho e tem a certeza que o altruísmo é a principal base para a construção de um mundo melhor.</p>

            </div>
            
          </div>

        </div>   

    </div>
    </div>

    <div class="col-md-4">
    <div class="members">
      <ul class="ch-grid">

            <li>
             <a style="cursor:pointer;"  id="person8">
              <div class="box-image_whoweare">
                 <img class="ch-item" src="images/quemsomos/8.jpg">
              </div>

              </a>
              <h3 class="h3color">Ricardo Henriques</h3>
              <p style="font-size:12px;">Advogado</p>
            </li>

        </ul>

         <!-- Modal -->
        <div id="myModal8" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <span class="close">×</span>
              <h2>Ricardo Henriques</h2>
            </div>
            <div class="modal-body">
                    <p class="litlebio">Ricardo Henriques, 24 anos, natural de Salvador. Advogado formado pela Universidade Católica do Salvador, fã de música e interessado em qualquer forma de manifestação cultural. Curioso em conhecer a história de vida das outras pessoas. Atualmente desenvolve trabalho voluntário no movimento Salvador pela Educação.</p>

            </div>
            
          </div>

        </div>   
    </div>
    </div>

    <br></br>
    <br></br>
    <br></br>
    <br></br>
  </div>
</div>
</div>

<script>
      // Get the modal
      var modal = document.getElementById('myModal1');
      var modal2 = document.getElementById('myModal2');
      var modal3 = document.getElementById('myModal3');
      var modal4 = document.getElementById('myModal4');
      var modal5 = document.getElementById('myModal5');
      var modal6 = document.getElementById('myModal6');
      var modal7 = document.getElementById('myModal7');
      var modal8 = document.getElementById('myModal8');

      // Get the button that opens the modal
      var btn = document.getElementById("person1");
      var btn2 = document.getElementById("person2");
      var btn3 = document.getElementById("person3");
      var btn4 = document.getElementById("person4");
      var btn5 = document.getElementById("person5");
      var btn6 = document.getElementById("person6");
      var btn7 = document.getElementById("person7");
      var btn8 = document.getElementById("person8");
      

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];
      var span2 = document.getElementsByClassName("close")[1];
      var span3 = document.getElementsByClassName("close")[2];
      var span4 = document.getElementsByClassName("close")[3];
      var span5 = document.getElementsByClassName("close")[4];
      var span6 = document.getElementsByClassName("close")[5];
      var span7 = document.getElementsByClassName("close")[6];
      var span8 = document.getElementsByClassName("close")[7]; 


      // When the user clicks the button, open the modal 
      btn.onclick = function() {
          modal.style.display = "block";
      }

      btn2.onclick = function() {
          modal2.style.display = "block";
      }

      btn3.onclick = function() {
          modal3.style.display = "block";
      }

      btn4.onclick = function() {
          modal4.style.display = "block";
      }

      btn5.onclick = function() {
          modal5.style.display = "block";
      }

      btn6.onclick = function() {
          modal6.style.display = "block";
      }

      btn7.onclick = function() {
          modal7.style.display = "block";
      }

      btn8.onclick = function() {
          modal8.style.display = "block";
      }

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
          modal.style.display = "none";
      }

      span2.onclick = function() {
         modal2.style.display = "none";
      }
      span3.onclick = function() {
          modal3.style.display = "none";
      }

      span4.onclick = function() {
         modal4.style.display = "none";
      }
      
      span5.onclick = function() {
          modal5.style.display = "none";
      }

      span6.onclick = function() {
         modal6.style.display = "none";
      }
      span7.onclick = function() {
          modal7.style.display = "none";
      }

      span8.onclick = function() {
         modal8.style.display = "none";
      }

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
          if (event.target == modal) {
             modal.style.display = "none";
             modal2.style.display = "none";
             modal3.style.display = "none";
             modal4.style.display = "none";
             modal5.style.display = "none";
             modal6.style.display = "none";
             modal7.style.display = "none";
             modal8.style.display = "none";
             
          }
      }
     

      // When the user clicks anywhere outside of the modal, close it
      
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('templates.ssatemplate', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>