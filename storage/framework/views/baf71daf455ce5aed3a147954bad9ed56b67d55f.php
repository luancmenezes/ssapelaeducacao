<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Salvador Pela Educação</title>
	<link rel="shortcut icon" href="/images/site/favicon.png">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="/css/stylee.css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.3.0/bootstrap.min.css" />
 <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" type='text/css'>
	<?php echo $__env->yieldContent('content-script'); ?>
	<!-- Fonts -->
    <!-- <link href='https://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'> -->
	<!-- <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'> -->
	<link href="/fonts/font-awesome.min.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
	<?php echo $__env->yieldContent('map-script'); ?>
</head>
<body >
	<div class="tudo">
	<!-- Luan Menezes-->
    <nav class="navbar navbar-default navbar-fixed-top menu-topo">
		<div class="container">
			<img id="logo-site" href="#"src="/images/site/logo-nome-azul.png" class="logo-menu" />
			<div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle drop-menu" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <!-- <a class="navbar-brand" href="#">WebSiteName</a> -->
			    </div>
			    <div class="collapse navbar-collapse " id="myNavbar">
			      <ul id="float-menu" class="nav navbar-nav opcoes-menu menu-principal">
			        <li class="active opcoes-menu"><a href="/">Home</a></li>
					<li class="opcoes-menu"><a href="/about">Sobre</a></li>
			        <li class="opcoes-menu"><a href="/actions">Ações</a></li>
			        <li class="opcoes-menu"><a href="/map">Mapa</a></li>
			        <li class="opcoes-menu"><a href="/whoweare">Quem Somos</a></li>
					<li class="opcoes-menu"><a href="/contact">Contato</a></li>
					<li> <a href="https://www.facebook.com/salvadorpelaeducacao" class="link-face"><img src="/images/site/facebook.png" class="img-responsive link-face"></a> </li>
					<?php echo $__env->yieldContent('extend'); ?>
			      </ul>
			    
			    </div>
			</div>
		</div>
	</nav>

		<div id="topo" class="col-md-12 col-sm-12"></div>

		<?php echo $__env->yieldContent('map'); ?>
		<div class="container">

				<?php echo $__env->yieldContent('content'); ?>

		</div>





	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<div class="footer-padding col-md-12 col-sm-12"></div>
	<footer class="footer">
	<div class="container">
    <div class="row">
			<div class="col-md-4">
                <center>
                  <img src="/images/site/mapa1.png" width="74" height="74" class="img-circle" alt="the-brains">
                  <br>
                  <h4 class="footertext">Endereço</h4>
                  <p class="footertext"> <i class="fa fa-map-marker" aria-hidden="true"></i> Salvador - Bahia<br>
                </center>
              </div>
              <div class="col-md-4">
                <center>
                  <img src="/images/site/mapa2.png" width="74" height="74" class="img-circle" alt="...">
                  <br>
                  <h4 class="footertext">Onde nos encontrar</h4>
                  <p class="footertext"><i class="fa fa-facebook-official" aria-hidden="true"></i><a id="facebook" href="https://www.facebook.com/salvadorpelaeducacao"> salvadorpelaeducacao</a>
                  </p>
                </center>
              </div>
              <div class="col-md-4">
                <center>
                  <img src="/images/site/mapa3.png" width="74" height="74" class="img-circle" alt="...">
                  <br>
                  <h4 class="footertext">Contato</h4>
                  <p class="footertext"><i class="fa fa-envelope" aria-hidden="true"></i> salvadorpelaeducacao@gmail.com<br>
                </center>
              </div>
            </div>
            <div class="row">
							<br>
            <p><center><a href="#">Desenvolvido por</a> <p class="footertext"><img src="/images/site/luan.png" width="50" height="33"></p></center></p>
        </div>
		</div>
	</div>
	</div>
	</footer>
	<?php echo $__env->yieldContent('footer-script'); ?>
</body>
</html>
